<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agree extends CB_Controller
{

    /**
     * 모델을 로딩합니다
     */
    protected $models = array('Board','Board_land','Post');

    /**
     * 헬퍼를 로딩합니다
     */
    protected $helpers = array('form', 'array');

    function __construct()
    {
        parent::__construct();

        /**
         * 라이브러리를 로딩합니다
         */
        $this->load->library(array('querystring'));
    }

    public function index()
    {

    }
    public function terms() {
        $this->load->view('board/header');
        $this->load->view('board/sub_header');
        $this->load->view('agree/terms');
        $this->load->view('board/footer');
    }
    public function privacy() {
        $this->load->view('board/header');
        $this->load->view('board/sub_header');
        $this->load->view('agree/privacy');
        $this->load->view('board/footer');
    }

}