<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Board extends CI_Controller {


    public function index()
    {

    }
    public function qna()
    {

        $qna_data =
            array(
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다가맹한다가맹한다가맹한다가맹한다가맹한다가맹한다가맹한다가맹한다가맹한다가맹한다가맹한다가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00019',
                    'content' => '가맹이다',
                    'replay' => '',
                    'name' => '홍길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00029',
                    'content' => '가맹이다옹',
                    'replay' => '',
                    'name' => '용길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00039',
                    'content' => '가맹',
                    'replay' => '1',
                    'name' => '황길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                )
            );
//        print_r($qna_data);
        $this->load->library('pagination');

        $config['base_url'] = 'http://land.test/board/qna';
        $config['total_rows'] = 200;
        $config['per_page'] = 10;
        $config['first_link'] = '처음';
        $config['last_link'] = '끝';
        $config['uri_segment'] = 3;
        $config['num_links'] = 4;
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';

        $this->pagination->initialize($config);
        $this->data['qna_data'] = $qna_data;

        $this->load->view('board/header');
        $this->load->view('board/sub_header');
        $this->load->view('board/qna');
        $this->load->view('board/footer');
    }
    public function notice()
    {



        $notice_data =
            array(
                array(
                    'write_num' => '00009',
                    'content' => '2020sus 1월 중국 베이징점 오픈!dddddddddddddddddddddddddddddddddddddddddddddddddddd',
                    'replay' => '1',
                    'name' => '오라클랜드',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '2020sus 1월 중국 베이징점 오픈!',
                    'replay' => '1',
                    'name' => '오라클랜드',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '2020sus 1월 중국 베이징점 오픈!',
                    'replay' => '1',
                    'name' => '오라클랜드',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '2020sus 1월 중국 베이징점 오픈!',
                    'replay' => '1',
                    'name' => '오라클랜드',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                ),
                array(
                    'write_num' => '00009',
                    'content' => '가맹한다',
                    'replay' => '1',
                    'name' => '고길동',
                    'date' => '2020.02.31',
                    'count' => '200'
                )
            );
//        print_r($qna_data);
        $this->data['notice_data'] = $notice_data;

        $this->load->view('board/header');
        $this->load->view('board/sub_header');
        $this->load->view('board/notice');
        $this->load->view('board/footer');
    }

    public function faq()
    {

        $faq_content =
            array(
                array(
                    'questions' => '가맹계약 후 개원까지의 소요 시일은 어떻게 되나요?',
                    'answer' => '2~3개월 정도 소요 예상됩니다.'
                ),
                array(
                    'questions' => '가맹 후 본사 지원은 무엇이 있나요',
                    'answer' => 'BI 매뉴얼 제공<br>
                                공동 마케팅<br>
                                통합 홈페이지 / 내부 인트라넷 제공<br>
                                시스템 매뉴얼 제공<br>
                                치료 노하우, 진료시스템, 운영시스템<br>
                                CS교육 및 경영지도를 위한 정기적 방문 <br>
                                의료장비 및 소모품 공동구매 혜택<br><br>
                                
                                (선택 사항)
                                <br><br>
                                전자차트 및 CRM 제공 / 인테리어 표준 폼 제공 / 장비 및 소모품 선정 제공 / 인사 및 세무관련 자문 '
                ),
                array(
                    'questions' => '직원교육은 어떻게 진행 되나요?',
                    'answer' => '본점 방문 교육 (1-2주) / 지점 방문 교육 (1-5일) * 지원 필요시'
                ),
                array(
                    'questions' => '가맹 계약기간은 몇 년인가요?',
                    'answer' => '1년이며 이후 동일조건으로 1년씩 갱신됩니다.'
                ),
                array(
                    'questions' => '현재 가맹이 불가능한 지역은 어디인가요?',
                    'answer' => ' 특별히 가맹이 불가능한 지역은 없지만, 기존 지점이 위치하고 있는 주변 지역을 피하는 것을 원칙으로 합니다.'
                )
            );

//                print_r($faq_content);
        $this->data['faq_content'] = $faq_content;

        $this->load->view('board/header');
        $this->load->view('board/sub_header');
        $this->load->view('board/faq');
        $this->load->view('board/footer');
    }

    public function notice_detail()
    {

        $notice_detail = array();

//                print_r($faq_content);
        $this->data['notice_detail'] = $notice_detail;

        $this->load->view('board/header');
        $this->load->view('board/sub_header');
        $this->load->view('board/notice_detail');
        $this->load->view('board/footer');
    }

    public function qna_write()
    {

        $notice_detail = array();

//                print_r($faq_content);
        $this->data['notice_detail'] = $notice_detail;

        $this->load->view('board/header');
        $this->load->view('board/sub_header');
        $this->load->view('board/qna_write');
        $this->load->view('board/footer');
    }
}
