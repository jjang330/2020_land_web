<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CB_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    
    /**
     * 모델을 로딩합니다
     */
    protected $models = array('Board','Board_land','Post');

    /**
     * 헬퍼를 로딩합니다
     */
    protected $helpers = array('form', 'array');

    function __construct()
    {
        parent::__construct();

        /**
         * 라이브러리를 로딩합니다
         */
        $this->load->library(array('querystring'));
    }

    public function index()
    {
        $this->load->view('company/layout');
        $this->load->view('company/header');
        $this->load->view('company/sub_header');
        $this->load->view('company/land_main');
        $this->load->view('company/footer');
    }

    public function about_us(){
        $this->load->view('company/layout');
        $this->load->view('board/header');
        $this->load->view('board/sub_header');
        $this->load->view('company/about_us');
        $this->load->view('board/footer');
    }


    public function affiliation(){

        $step__list = array(
            array(
                'step_img' => 'land_FR_step01.png',
                'step_description' => '본사 방문 및 파견상담'
            ),
            array(
                'step_img' => 'land_FR_step02.png',
                'step_description' => '타당성 검토 및 협의'
            ),
            array(
                'step_img' => 'land_FR_step03.png',
                'step_description' => '가맹계약서 작성'
            ),
            array(
                'step_img' => 'land_FR_step04.png',
                'step_description' => '인테리어 계약서 작성'
            ),
            array(
                'step_img' => 'land_FR_step05.png',
                'step_description' => '인테리어 공사 진행'
            ),
            array(
                'step_img' => 'land_FR_step06.png',
                'step_description' => '인허가 신고, 구인공고'
            ),
            array(
                'step_img' => 'land_FR_step07.png',
                'step_description' => '의사 및 스탭 OJT,<br>개원전 교육'
            ),
            array(
                'step_img' => 'land_FR_step08.png',
                'step_description' => '오픈행사 및<br>프로모션 지원'
            ),
            array(
                'step_img' => 'land_FR_step09.png',
                'step_description' => '정기적 운영 지원'
            )
        );

        $data['step__list'] = $step__list;

        $this->load->view('company/layout');
        $this->load->view('board/header',$data);
        $this->load->view('board/sub_header');
        $this->load->view('company/affiliation');
        $this->load->view('board/footer');
    }


    public function marketing(){

        $online = array(
            array(
                'img' => 'land_marketing_on01.png',
                'title' => '바이럴 마케팅',
                'content01' => '- 브랜드채널 운영',
                'content02' => '- 언론 홍보',
                'content03' => '',
                'content04' => '',
            ),
            array(
                'img' => 'land_marketing_on02.png',
                'title' => 'SNS마케팅',
                'content01' => '- 인스타그램',
                'content02' => '- 페이스북',
                'content03' => '- 유튜브',
                'content04' => '- 카카오톡 채널',
            ),
            array(
                'img' => 'land_marketing_on03.png',
                'title' => '디지털 마케팅',
                'content01' => '- 키워드 광고',
                'content02' => '- 배너 광고',
                'content03' => '',
                'content04' => '',
            ),
            array(
                'img' => 'land_marketing_on04.png',
                'title' => '홈페이지 제작',
                'content01' => '- 홈페이지',
                'content02' => '- 모바일 홈페이지',
                'content03' => '- 랜딩페이지',
                'content04' => '',
            )
        );
        $offline = array(
            array(
                'img' => 'land_marketing_off01.png',
                'title' => '콘텐츠 제작 마케팅',
                'content01' => '- 영상 촬영',
                'content02' => '- 인쇄물 제작',
                'content03' => '- 베너 제작',
                'content04' => '',
            ),
            array(
                'img' => 'land_marketing_off02.png',
                'title' => '오프라인 광고',
                'content01' => '- 옥외 광고',
                'content02' => '- 버스 광고',
                'content03' => '- 지하철 광고',
                'content04' => '',
            )
        );
        $data['online'] = $online;
        $data['offline'] = $offline;

        $this->load->view('company/layout');
        $this->load->view('board/header',$data);
        $this->load->view('board/sub_header');
        $this->load->view('company/marketing');
        $this->load->view('board/footer');
    }


    public function consulting(){

        $open = array(
            array(
                'img' => 'land_consulting_open01.png',
                'title' => '입지 상권분석',
                'content01' => '상권분석, 타당성 평가 등을 토한<br>개원입지를 컨설팅합니다.',
            ),
            array(
                'img' => 'land_consulting_open02.png',
                'title' => '인테리어',
                'content01' => '만은 지점 개원 경험에서 검증된<br>최적화된 인테리어를 제안합니다.',
            ),
            array(
                'img' => 'land_consulting_open03.png',
                'title' => '장비, 기자재',
                'content01' => '병원운영에 필요한 물품리스트 제공 및<br>예산과 실용성에 중점을 둔 기자재를<br>개원 규모에 맞게 납품합니다.',
            ),
            array(
                'img' => 'land_consulting_open04.png',
                'title' => '인력 채용',
                'content01' => '직원 모집부터 면접 진행 방법 등<br>채용기준을 제시하고 연봉,복리후생과<br>같은 인력운영 계획안을 제공합니다.',
            ),
            array(
                'img' => 'land_consulting_open05.png',
                'title' => '진료 서비스 교육',
                'content01' => '진료, 장비, 전산과 같은 기본 업무부터<br>고객 응대 및 고객 관리법을 교육합니다.',
            )
        );

        $employee_training = array(
            array(
                'img' => 'land_consulting_edu01.png',
                'title' => '진료 교육',
                'content01' => '- 피부과 질환 종류',
                'content02' => '- 병변별 적응증 치료방법',
                'content03' => '- 피부과 장비',
                'content04' => '',
            ),
            array(
                'img' => 'land_consulting_edu02.png',
                'title' => '상담 교육',
                'content01' => '- 다양한 상담케이스와 상담테크닉 공유',
                'content02' => '고객 유형별 관리 및 응대방법 공유',
                'content03' => '',
                'content04' => '',
            ),
             array(
                 'img' => 'land_consulting_edu03.png',
                 'title' => '처치 교육',
                 'content01' => '- 치료전후 준비사항',
                 'content02' => '- 어시스트방법 및 동선',
                 'content03' => '상황별 고객응대 교육',
                 'content04' => '',
             ),
            array(
                'img' => 'land_consulting_edu04.png',
                'title' => '피부관리 교육',
                'content01' => '- 피부타입별 적응증 관리 교육',
                'content02' => '- 관리실장비 및 스케일링 교육',
                'content03' => '- 관리 옵저베이션, 실무교육',
                'content04' => '- 관리시 고객응대 교육',
            ),
            array(
                'img' => 'land_consulting_edu05.png',
                'title' => '전자차트 교육',
                'content01' => '- 접수, 수납, 수납마감',
                'content02' => '- 보험청구',
                'content03' => '- 각정 제출 서류 발급',
                'content04' => 'CRM프로그램 교육',
            ),
            array(
                'img' => 'land_consulting_edu06.png',
                'title' => 'CS 교육',
                'content01' => '- CS 마인드 교육',
                'content02' => '- 인사, 표정, 복장',
                'content03' => '- MOT 관리',
                'content04' => '- 전화응대법',
                'content05' => '- 컴플레인 고객응대법',

            )
        );

        $medical_tourism = array(
            array(
                'img' => 'land_consulting_tour01.png',
                'title' => '의료관광 정책 및 법률',
                'content01' => '- 의료관광 절차 안내',
                'content02' => '- 의료관광 정책 및 법률안내',
                'content03' => '- 행정업무 및 리스크 이해',
            ),
            array(
                'img' => 'land_consulting_tour02.png',
                'title' => '의료관광 특화프로그램',
                'content01' => '- 외국인환자 맞춤형 진료상품 컨설팅',
                'content02' => '- 외국인 환자용 서식, 원내 안내물 제공',
                'content03' => '- 의료관광 서비스 프로그램 컨선팅',
            ),
            array(
                'img' => 'land_consulting_tour03.png',
                'title' => '외국인환자 진료 프로세스',
                'content01' => '- 개인, 단체별 진료 프로세스',
                'content02' => '- 치료별 세부방안 코치',
                'content03' => '- 파트별 전담인력 배치',
            ),
            array(
                'img' => 'land_consulting_tour04.png',
                'title' => '외국인환자 응대 메뉴얼',
                'content01' => '- 대상국가의 문화 특성 이해',
                'content02' => '- 진료 단계별 응대요령',
                'content03' => '- 컴플레인 대응요령',
            )
        );
        $data['open'] = $open;
        $data['employee_training'] = $employee_training;
        $data['medical_tourism'] = $medical_tourism;

        $this->load->view('company/layout');
        $this->load->view('board/header',$data);
        $this->load->view('board/sub_header');
        $this->load->view('company/consulting');
        $this->load->view('board/footer');
    }
}
