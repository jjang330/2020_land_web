<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Register class
 *
 * Copyright (c) CIBoard <www.ciboard.co.kr>
 *
 * @author CIBoard (develop@ciboard.co.kr)
 */

/**
 * 회원 가입과 관련된 controller 입니다.
 */
class Register extends CB_Controller
{

	/**
	 * 모델을 로딩합니다
	 */
	protected $models = array('Member_nickname', 'Member_meta', 'Member_auth_email', 'Member_userid');

	/**
	 * 헬퍼를 로딩합니다
	 */
	protected $helpers = array('form', 'array', 'string');

	function __construct()
	{
		parent::__construct();

		/**
		 * 라이브러리를 로딩합니다
		 */
		$this->load->library(array('querystring', 'form_validation', 'email', 'notelib', 'point'));

		if ( ! function_exists('password_hash')) {
			$this->load->helper('password');
		}
	}


	/**
	 * 회원 약관 동의시 작동하는 함수입니다
	 */
	public function index()
	{
		// 이벤트 라이브러리를 로딩합니다
		$eventname = 'event_register_index';
		$this->load->event($eventname);

		$view = array();
		$view['view'] = array();

		// 이벤트가 존재하면 실행합니다
		$view['view']['event']['before'] = Events::trigger('before', $eventname);

		if ($this->member->is_member()
			&& ! ($this->member->is_admin() === 'super' && $this->uri->segment(1) === config_item('uri_segment_admin'))) {
			redirect();
		}

		if ($this->cbconfig->item('use_register_block')) {

			// 이벤트가 존재하면 실행합니다
			$view['view']['event']['before_block_layout'] = Events::trigger('before_block_layout', $eventname);

			/**
			 * 레이아웃을 정의합니다
			 */
			$page_title = $this->cbconfig->item('site_meta_title_register');
			$meta_description = $this->cbconfig->item('site_meta_description_register');
			$meta_keywords = $this->cbconfig->item('site_meta_keywords_register');
			$meta_author = $this->cbconfig->item('site_meta_author_register');
			$page_name = $this->cbconfig->item('site_page_name_register');

			$layoutconfig = array(
				'path' => 'register',
				'layout' => 'layout',
				'skin' => 'register_block',
				'layout_dir' => $this->cbconfig->item('layout_register'),
				'mobile_layout_dir' => $this->cbconfig->item('mobile_layout_register'),
				'use_sidebar' => $this->cbconfig->item('sidebar_register'),
				'use_mobile_sidebar' => $this->cbconfig->item('mobile_sidebar_register'),
				'skin_dir' => $this->cbconfig->item('skin_register'),
				'mobile_skin_dir' => $this->cbconfig->item('mobile_skin_register'),
				'page_title' => $page_title,
				'meta_description' => $meta_description,
				'meta_keywords' => $meta_keywords,
				'meta_author' => $meta_author,
				'page_name' => $page_name,
			);
			$view['layout'] = $this->managelayout->front($layoutconfig, $this->cbconfig->get_device_view_type());
			$this->data = $view;
			$this->layout = element('layout_skin_file', element('layout', $view));
			$this->view = element('view_skin_file', element('layout', $view));

			return false;
		}

		/**
		 * 전송된 데이터의 유효성을 체크합니다
		 */
		$config = array(
			array(
				'field' => 'agree',
				'label' => '회원가입약관',
				'rules' => 'trim|required',
			),
			array(
				'field' => 'agree2',
				'label' => '개인정보취급방침',
				'rules' => 'trim|required',
			),
		);
		$this->form_validation->set_rules($config);

		/**
		 * 유효성 검사를 하지 않는 경우, 또는 유효성 검사에 실패한 경우입니다.
		 * 즉 글쓰기나 수정 페이지를 보고 있는 경우입니다
		 */
		if ($this->form_validation->run() === false) {

			// 이벤트가 존재하면 실행합니다
			$view['view']['event']['formrunfalse'] = Events::trigger('formrunfalse', $eventname);

			$this->session->set_userdata('registeragree', '');

			$view['view']['member_register_policy1'] = $this->cbconfig->item('member_register_policy1');
			$view['view']['member_register_policy2'] = $this->cbconfig->item('member_register_policy2');
			$view['view']['canonical'] = site_url('register');

			// 이벤트가 존재하면 실행합니다
			$view['view']['event']['before_layout'] = Events::trigger('before_layout', $eventname);

			/**
			 * 레이아웃을 정의합니다
			 */
			$page_title = $this->cbconfig->item('site_meta_title_register');
			$meta_description = $this->cbconfig->item('site_meta_description_register');
			$meta_keywords = $this->cbconfig->item('site_meta_keywords_register');
			$meta_author = $this->cbconfig->item('site_meta_author_register');
			$page_name = $this->cbconfig->item('site_page_name_register');

			$layoutconfig = array(
				'path' => 'register',
				'layout' => 'layout',
				'skin' => 'register',
				'layout_dir' => $this->cbconfig->item('layout_register'),
				'mobile_layout_dir' => $this->cbconfig->item('mobile_layout_register'),
				'use_sidebar' => $this->cbconfig->item('sidebar_register'),
				'use_mobile_sidebar' => $this->cbconfig->item('mobile_sidebar_register'),
				'skin_dir' => $this->cbconfig->item('skin_register'),
				'mobile_skin_dir' => $this->cbconfig->item('mobile_skin_register'),
				'page_title' => $page_title,
				'meta_description' => $meta_description,
				'meta_keywords' => $meta_keywords,
				'meta_author' => $meta_author,
				'page_name' => $page_name,
			);
			$view['layout'] = $this->managelayout->front($layoutconfig, $this->cbconfig->get_device_view_type());
			$this->data = $view;
			$this->layout = element('layout_skin_file', element('layout', $view));
			$this->view = element('view_skin_file', element('layout', $view));

		} else {
			/**
			 * 유효성 검사를 통과한 경우입니다.
			 * 즉 데이터의 insert 나 update 의 process 처리가 필요한 상황입니다
			 */

			// 이벤트가 존재하면 실행합니다
			$view['view']['event']['formruntrue'] = Events::trigger('formruntrue', $eventname);

			$this->session->set_userdata('registeragree', '1');
			redirect('register/form');
		}
	    $agree = array(
                'terms' => '<article class="agree__container">
    <section class="agree__box">
        <div>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;text-align:center;font-weight:bold;font-size:27px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">&nbsp;</span><span style="font-weight:bold;font-size:27px;">오라클랜드</span><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">(</span><span style="font-weight:bold;font-size:27px;">주</span><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">)&nbsp;</span><span style="font-weight:bold;font-size:27px;">이용약관</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">1</span><span style="font-weight:bold;font-size:15px;">장 총칙</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">1</span>조 목적</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>이 이용약관은 오라클랜드<span style="font-family:함초롬바탕;">(</span>주<span style="font-family:함초롬바탕;">)(</span>이하&nbsp;<span style="font-family:함초롬바탕;">‘</span>회사<span style="font-family:함초롬바탕;">’</span>라 한다<span style="font-family:함초롬바탕;">)</span>가 제공하는 오라클랜드<span style="font-family:함초롬바탕;">(</span>주<span style="font-family:함초롬바탕;">)&nbsp;</span>사업 관련 온라인 서비스<span style="font-family:함초롬바탕;">(</span>
            <a href="www.oracleland.net"><u><span style="font-family:함초롬바탕;color:#0000ff;">www.oracleland.net</span></u></a><span style="font-family:함초롬바탕;">)&nbsp;</span>이용과 관련하여 이용조건 및 절차<span style="font-family:함초롬바탕;">,&nbsp;</span>기타 필요한 사항을 규정함을 목적으로 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">2</span>조 정의</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>이 이용약관에서 사용하는 용어의 정의는 다음과 같습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1. ‘</span>이용자<span style="font-family:함초롬바탕;">’</span>라 함은 회사의 사이트에 접속하여 이 약관에 따라 회사가 제공하는 서비스를 이용하는 자를 말합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2. ‘</span>서비스<span style="font-family:함초롬바탕;">’</span>라 함은 구현되는 단말기<span style="font-family:함초롬바탕;">(PC, TV,&nbsp;</span>휴대형단말기 등의 각종 유무선 장치를 포함<span style="font-family:함초롬바탕;">)</span>와 상관없이 이용자가 이용할 수 있는 오라클랜드<span style="font-family:함초롬바탕;">(</span>주<span style="font-family:함초롬바탕;">)&nbsp;</span>및 웹사이트&nbsp;<span style="font-family:함초롬바탕;">(www.oracleland.net)&nbsp;</span>관련 제반 서비스를 의미합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">3. ‘</span>게시물<span style="font-family:함초롬바탕;">’</span>이라 함은 서비스상에 게시한 부호<span style="font-family:함초롬바탕;">,&nbsp;</span>문자<span style="font-family:함초롬바탕;">,&nbsp;</span>음성<span style="font-family:함초롬바탕;">,&nbsp;</span>음향<span style="font-family:함초롬바탕;">,&nbsp;</span>화상<span style="font-family:함초롬바탕;">,&nbsp;</span>동영상 등의 정보 형태의 등록<span style="font-family:함초롬바탕;">,&nbsp;</span>제작된 글<span style="font-family:함초롬바탕;">,&nbsp;</span>사진 동영상 및 각종 파일과 링크 등을 의미합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">3</span>조 약관의 효력과 변경</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>이 약관은 이용자에게 공시함으로서 효력이 발생합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>회사는 사정 변경의 경우와 영업상 중요사유가 있을 때 약관을 변경할 수 있으며<span style="font-family:함초롬바탕;">,&nbsp;</span>변경된 약관은 전항과 같은 방법으로 효력이 발생합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">4</span>조 약관 외 준칙</p>
            <p>이 약관에 명시되지 않은 사항이 관계법령에 규정되어 있을 경우에는 그 규정에 따릅니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">2</span><span style="font-weight:bold;font-size:15px;">장 서비스 이용시간</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>서비스 이용시간은 업무상 또는 기술상 특별한 지장이 없는 한 연중무휴&nbsp;<span style="font-family:함초롬바탕;">1</span>일&nbsp;<span style="font-family:함초롬바탕;">24</span>시간을 원칙으로 합니다<span style="font-family:함초롬바탕;">.&nbsp;</span>단<span style="font-family:함초롬바탕;">,&nbsp;</span>사이트는 시스템 정기점검<span style="font-family:함초롬바탕;">,&nbsp;</span>증설 및 교체를 위해 사이트가 정한 날이나 시간에 서비스를 일시중단 할 수 있으며 예정된 작업으로 인한 서비스 일시 중단은 사이트의 홈페이지에 사전에 공지하오니 수시로 참고하시길 바랍니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>단<span style="font-family:함초롬바탕;">,&nbsp;</span>사이트는 다음 경우에 대하여 사전 공지나 예고 없이 서비스를 일시적 혹은 영구적으로 중단할 수 있습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">(1)&nbsp;</span>긴급한 시스템 점검<span style="font-family:함초롬바탕;">,&nbsp;</span>증설<span style="font-family:함초롬바탕;">,&nbsp;</span>교체<span style="font-family:함초롬바탕;">,&nbsp;</span>고장 혹은 오동작을 일으키는 경우</p>
            <p><span style="font-family:함초롬바탕;">(2)&nbsp;</span>국가비상사태<span style="font-family:함초롬바탕;">,&nbsp;</span>정전<span style="font-family:함초롬바탕;">,&nbsp;</span>천재지변 등의 불가항력적인 사유가 있는 경우</p>
            <p><span style="font-family:함초롬바탕;">(3)&nbsp;</span>전기통신사업법에 규정된 기간통신사업자가 전기통신 서비스를 중지한 경우</p>
            <p><span style="font-family:함초롬바탕;">(4)&nbsp;</span>서비스 이용의 폭주 등으로 정상적인 서비스 이용에 지장이 있는 경우</p>
            <p><span style="font-family:함초롬바탕;">3.&nbsp;</span>전항에 의한 서비스 중단의 경우 사이트는 사전에 공지사항 등을 통하여 회원에게 통지 합니다<span style="font-family:함초롬바탕;">.&nbsp;</span>단<span style="font-family:함초롬바탕;">,&nbsp;</span>사이트가 통제할 수 없는 사유로 발생한 서비스의 중단에 대하여 사전공지가 불가능한 경우에는 사후공지로 대신합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">3</span><span style="font-weight:bold;font-size:15px;">장 서비스에 관한 책임의 제한</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>회사는 서비스의 이용자들의 상담내용이 관리자를 제외한 제<span style="font-family:함초롬바탕;">3</span>자에게 유출되지 않도록 최선을 다해 보안을 유지하려고 노력합니다<span style="font-family:함초롬바탕;">.&nbsp;</span>그러나 다음과 같은 경우에는 상담 내용 공개 및 상실에 대하여 회사의 책임이 없습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">(1)&nbsp;</span>사용자의 부주의로 인해 상담내용이 공개되는 경우</p>
            <p><span style="font-family:함초롬바탕;">(2)&nbsp;</span>사용자가&nbsp;<span style="font-family:함초롬바탕;">\'</span>삭제<span style="font-family:함초롬바탕;">\'&nbsp;</span>기능을 사용하여 상담을 삭제하였을 경우</p>
            <p><span style="font-family:함초롬바탕;">(3)&nbsp;</span>천재지변이나 그 밖의 회사에서 통제할 수 없는 상황에 의하여 상담내용이 공개되거나 상담내용이 상실되었을 경우</p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>아래와 같은 상담을 신청하는 경우에는 온라인상담 전체 또는 일부 제공하지 않을 수 있습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">(1)&nbsp;</span>같은 내용의 상담을 반복하여 신청하는 경우</p>
            <p><span style="font-family:함초롬바탕;">(2)&nbsp;</span>상식에 어긋나는 표현을 사용하여 상담을 신청하는 경우</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">4</span><span style="font-weight:bold;font-size:15px;">장 의무</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">1</span>조 회사의 의무</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>회사는 특별한 사정이 없는 한 이용자가 서비스를 이용할 수 있도록 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>회사는 이 약관에서 정한 바에 따라 계속적<span style="font-family:함초롬바탕;">,&nbsp;</span>안정적으로 서비스를 제공할 의무가 있습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">3.&nbsp;</span>회사는 이용자로부터 소정의 절차에 의해 제기되는 의견에 대해서 적절한 절차를 거쳐 처리하며<span style="font-family:함초롬바탕;">,&nbsp;</span>처리 시 일정기간이 소요될 경우 이용자에게 그 사유와 처리 일정을 알려주어야 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">2</span>조 이용자의 의무</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>이용자는 다음 행위를 하여서는 안 됩니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">(1)&nbsp;</span>상담신청 또는 변경 시 허위내용의 기재</p>
            <p><span style="font-family:함초롬바탕;">(2)&nbsp;</span>타인의 정보도용</p>
            <p><span style="font-family:함초롬바탕;">(3)&nbsp;</span>회사에 게시된 정보의 변경</p>
            <p><span style="font-family:함초롬바탕;">(4)&nbsp;</span>회사와 기타 제<span style="font-family:함초롬바탕;">3</span>자의 저작권 등 지적재산권에 대한 침해</p>
            <p><span style="font-family:함초롬바탕;">(5)&nbsp;</span>회사 및 기타 제<span style="font-family:함초롬바탕;">3</span>자의 명예를 손상시키거나 업무를 방해하는 행위</p>
            <p><span style="font-family:함초롬바탕;">(6)&nbsp;</span>외설　또는 폭력적인 말이나 글<span style="font-family:함초롬바탕;">,&nbsp;</span>화상<span style="font-family:함초롬바탕;">,&nbsp;</span>음향<span style="font-family:함초롬바탕;">,&nbsp;</span>기타 공서양속에 반하는 정보를 회사의 사이트에 공개 또는 게시하는 행위</p>
            <p><span style="font-family:함초롬바탕;">(7)&nbsp;</span>기타 불법적이거나 부당한 행위</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">5</span><span style="font-weight:bold;font-size:15px;">장 게시물의 삭제</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>회사는 게시판에 정보통신망이용촉진 및 정보보호 등에 관한 법률을 위반한 청소년유해매체물이 게시되어 있는 경우에는 이를 지체 없이 삭제 합니다<span style="font-family:함초롬바탕;">.&nbsp;</span>다만<span style="font-family:함초롬바탕;">, 19</span>세 이상의&nbsp;<span style="font-family:함초롬바탕;">“</span>이용자<span style="font-family:함초롬바탕;">”</span>만 이용할 수 있는 게시판은 예외로 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>회사가 운영하는 게시판 등에 게시된 정보로 인하여 법률상 이익이 침해된 자는 회사에게 당해 정보의 삭제 또는 반박내용의 게재를 요청할 수 있습니다<span style="font-family:함초롬바탕;">.&nbsp;</span>이 경우 회사는 지체 없이 필요한 조치를 취하고 이를 즉시 신청인에게 통지합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">6</span><span style="font-weight:bold;font-size:15px;">장 저작권 등의 귀속</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>회사가 작성한 게시물 또는 저작물에 대한 저작권 기타 지적재산권은 회사에 귀속합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>이용자는 회사가 제공하는 서비스를 이용함으로써 얻은 정보 중 회사에 지적재산권이 귀속된 정보를 회사의 사전승낙 없이 복제<span style="font-family:함초롬바탕;">,&nbsp;</span>전송<span style="font-family:함초롬바탕;">,&nbsp;</span>출판<span style="font-family:함초롬바탕;">,&nbsp;</span>배포<span style="font-family:함초롬바탕;">,&nbsp;</span>방송 기타 방법에 의하여 영리목적으로 이용하거나 제<span style="font-family:함초롬바탕;">3</span>자에게 이용하게 하여서는 안 됩니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">3.&nbsp;</span>회사는 약정에 따라 이용자의 저작물을 사용하는 경우 당해 이용자의 허락을 받습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">7</span><span style="font-weight:bold;font-size:15px;">장 분쟁조정</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>본 이용약관에 규정된 것을 제외하고 발생하는 서비스 이용에 관한 제반문제에 관한 분쟁은 최대한 쌍방합의에 의해 해결하도록 한다<span style="font-family:함초롬바탕;">.</span></p>
            <p style="font-weight:bold;"><span style="font-family:함초롬바탕;">2.&nbsp;</span>서비스 이용으로 발생한 분쟁에 대해 소송이 제기될 경우 회사의 소재지를 관할하는 법원을 관할법원으로 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>부칙</p>
            <p>이 약관은&nbsp;<span style="font-family:함초롬바탕;">2020</span>년&nbsp;<span style="font-family:함초롬바탕;">03</span>월&nbsp;<span style="font-family:함초롬바탕;">12</span>일 시행합니다<span style="font-family:함초롬바탕;">.</span></p>
        </div>
    </section>
</article>',
                'privacy' => '<article class="agree__container">
    <section class="agree__box">
        <div>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;text-align:center;font-weight:bold;font-size:27px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">&nbsp;</span><span style="font-weight:bold;font-size:27px;">오라클랜드</span><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">(</span><span style="font-weight:bold;font-size:27px;">주</span><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">)&nbsp;</span><span style="font-weight:bold;font-size:27px;">개인정보 처리방침</span></p>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">오라클랜드</span><span style="font-family:휴먼명조;font-size:16px;">(</span><span style="font-size:16px;">주</span><span style="font-family:휴먼명조;font-size:16px;">) (</span><span style="font-size:16px;">이하&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">‘</span><span style="font-size:16px;">회사</span><span style="font-family:휴먼명조;font-size:16px;">’</span><span style="font-size:16px;">라 한다</span><span style="font-family:휴먼명조;font-size:16px;">)</span><span style="font-size:16px;">는 개인정보 보호법 제</span><span style="font-family:휴먼명조;font-size:16px;">30</span><span style="font-size:16px;">조에 따라 정보주체의 개인정보를&nbsp;</span><span style="font-size:16px;">보호하고 이와 관련한 고충을 신속하고 원활하게 처리할 수 있도록 하기 위하여</span><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">다음과 같이 개인정보 처리지침을 수립</span><span style="font-size:16px;">․</span><span style="font-size:16px;">공개합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;font-size:16px;"><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">1</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">개인정보의 처리목적</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)</span><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">회사는 다음의 목적을 위하여 개인정보를 처리합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span><span style="font-size:16px;">처리하고 있는 개인정보는 다음의 목적 이외의 용도로는 이용되지 않으며</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">이용 목적이 변경되는 경우에는 개인정보 보호법 제</span><span style="font-family:휴먼명조;font-size:16px;">18</span><span style="font-size:16px;">조에 따라 별도의 동의를 받는 등 필요한 조치를 이행할 예정입니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-right:10.2pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-right:10.2pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">1.&nbsp;</span><span style="font-size:16px;">서비스 이용</span></p>
            <p style="margin-right:10.2pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">서비스 이용에 대한 견적 및 상담 진행을 위한 기본적인 대상자 정보</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">문의사항에 대한 답변을 전달하기 위한 원활한 의사소통 등을 목적으로 개인정보를 처리합니다</span><span style="font-family:휴먼명조;font-size:16px;">.</span></p>
            <p style="margin-left:18.0pt;margin-right:10.2pt;text-indent:-18.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">2</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">개인정보의 처리 및 보유기간</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)&nbsp;</span><span style="font-size:16px;">①</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-size:16px;">회사는 법령에 따른 개인정보 보유</span><span style="font-size:16px;">․</span><span style="font-size:16px;">이용기간 또는 정보주체로부터 개인정보를 수집 시에 동의받은 개인정보 보유</span><span style="font-size:16px;">․</span><span style="font-size:16px;">이용기간 내에서 개인정보를 처리</span><span style="font-size:16px;">․</span><span style="font-size:16px;">보유합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="line-height:90%;margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">②&nbsp;</span><span style="font-size:16px;">각각의 개인정보 처리 및 보유 기간은 다음과 같습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="line-height:80%;margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">1.&nbsp;</span><span style="font-size:16px;">서비스 이용&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">개인정보의 수집 또는 제공받은 목적 달성 시 지체 없 이 파기합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span><span style="font-size:16px;">다만</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">정보제공자가 개인정보 삭제를 요청할 경우 즉시 삭제합니다</span><span style="font-family:휴먼명조;font-size:16px;">.</span></p>
            <p style="margin-left:46.0pt;margin-right:10.2pt;text-indent:-46.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">3</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">정보주체의 권리</span><span style="font-weight:bold;font-size:16px;">․</span><span style="font-weight:bold;font-size:16px;">의무 및 행사방법</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)</span><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">①&nbsp;</span><span style="font-size:16px;">정보주체는 회사에&nbsp;</span><span style="font-size:16px;">대해 언제든지 다음 각 호의 개인정보 보호 관련 권리를 행사할 수 있습니다</span><span style="font-family:휴먼명조;font-size:16px;">.</span><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">1.&nbsp;</span><span style="font-size:16px;">개인정보 열람요구</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">2.&nbsp;</span><span style="font-size:16px;">오류 등이 있을 경우 정정 요구</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">3.&nbsp;</span><span style="font-size:16px;">삭제요구&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">4.&nbsp;</span><span style="font-size:16px;">처리정지 요구&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">②&nbsp;</span><span style="font-size:16px;">제</span><span style="font-family:휴먼명조;font-size:16px;">1</span><span style="font-size:16px;">항에 따른 권리 행사는 회사에 대해 서면</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">전화</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">전자우편</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">모사전송</span><span style="font-family:휴먼명조;font-size:16px;">(FAX)&nbsp;</span><span style="font-size:16px;">등을 통하여 하실 수 있으며 회사는 이에 대해 지체없이 조치하겠습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">③&nbsp;</span><span style="font-size:16px;">정보주체가 개인정보의 오류 등에 대한 정정 또는 삭제를 요구한 경우에는 회사는 정정 또는 삭제를 완료할 때까지 당해 개인정보를 이용하거나 제공하지 않습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">④&nbsp;</span><span style="font-size:16px;">제</span><span style="font-family:휴먼명조;font-size:16px;">1</span><span style="font-size:16px;">항에 따른 권리 행사는 정보주체의 법정대리인이나 위임을 받은 자 등 대리인을 통하여 하실 수 있습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span><span style="font-size:16px;">이 경우 개인정보 보호법 시행규칙 별지 제</span><span style="font-family:휴먼명조;font-size:16px;">11</span><span style="font-size:16px;">호 서식에 따른 위임장을 제출하셔야 합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">⑤&nbsp;</span><span style="font-size:16px;">정보주체는 개인정보 보호법 등 관계법령을 위반하여 회사가 처리하고 있는 정보주체 본인이나 타인의 개인정보 및 사생활을 침해하여서는 아니됩니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">4</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">처리하는 개인정보 항목</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)</span><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">회사는 다음의 개인정보 항목을 처리하고 있습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">1.&nbsp;</span><span style="font-size:16px;">서비스 제공&nbsp;</span></p>
            <p style="margin-left:101.9pt;margin-right:10.2pt;text-indent:-101.9pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">․</span><span style="font-size:16px;">필수항목&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">성명</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">연락처</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">5</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">개인정보의 파기</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)&nbsp;</span><span style="font-size:16px;">①&nbsp;</span><span style="font-size:16px;">회사는 개인정보 보유기간의 경과</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">처리목적 달성 등 개인정보가 불필요하게 되었을 때에는 지체없이 해당 개인정보를 파기합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">②&nbsp;</span><span style="font-size:16px;">정보주체로부터 동의받은 개인정보 보유기간이 경과하거나 처리목적이 달성되었음에도 불구하고 다른 법령에 따라 개인정보를 계속 보존하여야 하는 경우에는</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">해당 개인정보를 별도의 데이터베이스</span><span style="font-family:휴먼명조;font-size:16px;">(DB)</span><span style="font-size:16px;">로 옮기거나 보관장소를 달리하여 보존합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">③&nbsp;</span><span style="font-size:16px;">개인정보 파기의 절차 및 방법은 다음과 같습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">1.&nbsp;</span><span style="font-size:16px;">파기절차&nbsp;</span></p>
            <p style="margin-left:24.0pt;margin-right:10.2pt;text-indent:-24.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">회사는 파기 사유가 발생한 개인정보를 선정하고</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">회사의 개인정보 보호책임자의 승인을 받아 개인정보를 파기합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">2.&nbsp;</span><span style="font-size:16px;">파기방법&nbsp;</span></p>
            <p style="margin-left:24.0pt;margin-right:10.2pt;text-indent:-24.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">회사는 전자적 파일 형태로 기록</span><span style="font-size:16px;">․</span><span style="font-size:16px;">저장된 개인정보는 기록을 재생할 수 없도록 기술적인 방법 또는 물리적인 방법을 이용하여 파기하며</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">종이 문서에 기록</span><span style="font-size:16px;">․</span><span style="font-size:16px;">저장된 개인정보는 분쇄기로 분쇄하거나 소각 등을 통하여 파기합니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:24.0pt;margin-right:10.2pt;text-indent:-24.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">6</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">개인정보의 안전성 확보조치</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)&nbsp;</span><span style="font-size:16px;">회사는 개인정보의 안전성 확보를 위해 다음과 같은 조치를 취하고 있습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">1.&nbsp;</span><span style="font-size:16px;">관리적 조치&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">내부관리계획 수립</span><span style="font-size:16px;">․</span><span style="font-size:16px;">시행</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">정기적 직원 교육 등&nbsp;</span></p>
            <p style="margin-left:34.4pt;margin-right:10.2pt;text-indent:-34.4pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">2.&nbsp;</span><span style="font-size:16px;">기술적 조치&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">개인정보처리시스템 등의 접근권한 관리</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">접근통제시스템 설치</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">고유식별정보 등의 암호화</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">3.&nbsp;</span><span style="font-size:16px;">물리적 조치&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">전산실</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">자료보관실 등의 접근통제&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">7</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">개인정보 보호책임자</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)&nbsp;</span><span style="font-size:16px;">①&nbsp;</span><span style="font-size:16px;">회사는 개인정보 처리에 관한 업무를 총괄해서 책임지고</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">개인정보 처리와 관련한 정보주체의 불만처리 및 피해구제 등을 위하여 아래와 같이 개인정보 보호책임자를 지정하고 있습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">▶&nbsp;</span><span style="font-size:16px;">개인정보 관리책임자&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">성명&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">박성호&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">직책&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">부장</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">연락처&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: 02-415-8974</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">▶&nbsp;</span><span style="font-size:16px;">개인정보 관리담당자</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">성명&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">사재훈&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">직책&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">팀장&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">연락처&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: 02-415-8974</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">②&nbsp;</span><span style="font-size:16px;">정보주체께서는 회사의 서비스</span><span style="font-family:휴먼명조;font-size:16px;">(</span><span style="font-size:16px;">또는 사업</span><span style="font-family:휴먼명조;font-size:16px;">)</span><span style="font-size:16px;">을 이용하시면서 발생한 모든 개인정보 보호 관련 문의</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">불만처리</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">피해구제 등에 관한 사항을 개인정보 보호책임자 및 담당부서로 문의하실 수 있습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span><span style="font-size:16px;">회사는 정보주체의 문의에 대해 지체없이 답변 및 처리해드릴 것입니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">8</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">개인정보 열람청구</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)&nbsp;</span><span style="font-size:16px;">정보주체는 개인정보 보호법 제</span><span style="font-family:휴먼명조;font-size:16px;">35</span><span style="font-size:16px;">조에 따른 개인정보의 열람 청구를 아래의 부서에 할 수 있습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span><span style="font-size:16px;">회사는 정보주체의 개인정보 열람청구가 신속하게 처리되도록 노력하겠습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">▶&nbsp;</span><span style="font-size:16px;">개인정보 열람청구 접수</span><span style="font-size:16px;">․</span><span style="font-size:16px;">처리 담당자</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">부서명&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">개발팀</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">담당자&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">사재훈 팀장</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">연락처&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: 02-415-8974</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">9</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">권익침해 구제방법</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)&nbsp;</span><span style="font-size:16px;">정보주체는 아래의 기관에 대해 개인정보 침해에 대한 피해구제</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">상담 등을 문의하실 수 있습니다</span><span style="font-family:휴먼명조;font-size:16px;">.&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:19.3pt;margin-right:10.2pt;text-indent:-19.3pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">&lt;</span><span style="font-size:16px;">아래의 기관은 회사와는 별개의 기관으로서</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">회사의 자체적인</span><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">개인</span><span style="font-size:16px;">정보 불만처리</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">피해구제 결과에 만족하지 못하시거나 보다 자세한 도움이</span><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">필요하시면 문의하여 주시기 바랍니다</span><span style="font-family:휴먼명조;font-size:16px;">&gt;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">▶&nbsp;</span><span style="font-size:16px;">개인정보 침해신고센터&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">(</span><span style="font-size:16px;">한국인터넷진흥원 운영</span><span style="font-family:휴먼명조;font-size:16px;">)&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">-&nbsp;</span><span style="font-size:16px;">소관업무&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">개인정보 침해사실 신고</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">상담 신청&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">-&nbsp;</span><span style="font-size:16px;">홈페이지&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: privacy.kisa.or.kr&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">-&nbsp;</span><span style="font-size:16px;">전화&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: (</span><span style="font-size:16px;">국번없이</span><span style="font-family:휴먼명조;font-size:16px;">) 118&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">-&nbsp;</span><span style="font-size:16px;">주소&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:</span><span style="font-family:휴먼명조;font-size:15px;">&nbsp;</span><span style="font-family:한컴바탕;">(58324)&nbsp;</span>전남 나주시 진흥길&nbsp;<span style="font-family:한컴바탕;">9(</span>빛가람동&nbsp;<span style="font-family:한컴바탕;">301-2) 3</span>층 개인정보침해신고센터</p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">▶&nbsp;</span><span style="font-size:16px;">개인정보 분쟁조정위원회</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">-&nbsp;</span><span style="font-size:16px;">소관업무&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-size:16px;">개인정보 분쟁조정신청</span><span style="font-family:휴먼명조;font-size:16px;">,&nbsp;</span><span style="font-size:16px;">집단분쟁조정&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">(</span><span style="font-size:16px;">민사적 해결</span><span style="font-family:휴먼명조;font-size:16px;">)&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">-&nbsp;</span><span style="font-size:16px;">홈페이지&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: www.kopico.go.kr&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">-&nbsp;</span><span style="font-size:16px;">전화&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: (</span><span style="font-size:16px;">국번없이</span><span style="font-family:휴먼명조;font-size:16px;">) 1833-6972</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:15px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">-&nbsp;</span><span style="font-size:16px;">주소&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">:&nbsp;</span><span style="font-family:한컴바탕;font-size:15px;">(03171)</span><span style="font-size:15px;">서울특별시 종로구 세종대로&nbsp;</span><span style="font-family:한컴바탕;font-size:15px;">209&nbsp;</span><span style="font-size:15px;">정부서울청사&nbsp;</span><span style="font-family:한컴바탕;font-size:15px;">4</span><span style="font-size:15px;">층</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">▶&nbsp;</span><span style="font-size:16px;">대검찰청 사이버범죄수사단&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: 02-3480-3573 (</span>
                <a href="http://www.spo.go.kr"><u><span style="font-family:휴먼명조;font-size:16px;">www.spo.go.kr</span></u></a><u><span style="font-family:휴먼명조;font-size:16px;">)</span></u></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span><span style="font-size:16px;">▶&nbsp;</span><span style="font-size:16px;">경찰청 사이버안전국&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">: 182 (http://cyberbureau.police.go.kr)</span></p>
            <p style="line-height:155%;margin-left:33.0pt;margin-right:10.2pt;text-indent:-33.0pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-weight:bold;font-size:16px;">제</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">10</span><span style="font-weight:bold;font-size:16px;">조</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">(</span><span style="font-weight:bold;font-size:16px;">개인정보 처리방침 변경</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">)&nbsp;</span><span style="font-size:16px;">①</span><span style="font-family:휴먼명조;font-weight:bold;font-size:16px;">&nbsp;</span><span style="font-size:16px;">이 개인정보 처리방침은&nbsp;</span><span style="font-family:휴먼명조;font-size:16px;">2020. 03. 12</span><span style="font-size:16px;">부터 적용됩니다</span><span style="font-family:휴먼명조;font-size:16px;">.</span></p>
            <p style="margin-left:18.6pt;margin-right:10.2pt;text-indent:-18.6pt;font-size:16px;"><span style="font-family:휴먼명조;font-size:16px;">&nbsp;</span></p>
            <p><span style="font-family:한컴바탕;">&nbsp;</span></p>
        </div>
    </section>
</article>'
        );
//        echo $agree['privacy'];
        $this->data['agree'] = $agree;
        $this->load->view('board/header',$agree);

	}


	/**
	 * 회원가입 폼 페이지입니다
	 */
	public function form()
	{
		// 이벤트 라이브러리를 로딩합니다
		$eventname = 'event_register_form';
		$this->load->event($eventname);

		if ($this->member->is_member() && ! ($this->member->is_admin() === 'super' && $this->uri->segment(1) === config_item('uri_segment_admin'))) {
			redirect();
		}

		$view = array();
		$view['view'] = array();

		// 이벤트가 존재하면 실행합니다
		$view['view']['event']['before'] = Events::trigger('before', $eventname);

		if ($this->cbconfig->item('use_register_block')) {

			// 이벤트가 존재하면 실행합니다
			$view['view']['event']['before_block_layout'] = Events::trigger('before_block_layout', $eventname);

			/**
			 * 레이아웃을 정의합니다
			 */
			$page_title = $this->cbconfig->item('site_meta_title_register_form');
			$meta_description = $this->cbconfig->item('site_meta_description_register_form');
			$meta_keywords = $this->cbconfig->item('site_meta_keywords_register_form');
			$meta_author = $this->cbconfig->item('site_meta_author_register_form');
			$page_name = $this->cbconfig->item('site_page_name_register_form');

			$layoutconfig = array(
				'path' => 'register',
				'layout' => 'layout',
				'skin' => 'register_block',
				'layout_dir' => $this->cbconfig->item('layout_register'),
				'mobile_layout_dir' => $this->cbconfig->item('mobile_layout_register'),
				'use_sidebar' => $this->cbconfig->item('sidebar_register'),
				'use_mobile_sidebar' => $this->cbconfig->item('mobile_sidebar_register'),
				'skin_dir' => $this->cbconfig->item('skin_register'),
				'mobile_skin_dir' => $this->cbconfig->item('mobile_skin_register'),
				'page_title' => $page_title,
				'meta_description' => $meta_description,
				'meta_keywords' => $meta_keywords,
				'meta_author' => $meta_author,
				'page_name' => $page_name,
			);
			$view['layout'] = $this->managelayout->front($layoutconfig, $this->cbconfig->get_device_view_type());
			$this->data = $view;
			$this->layout = element('layout_skin_file', element('layout', $view));
			$this->view = element('view_skin_file', element('layout', $view));
			return false;
		}


		$password_length = $this->cbconfig->item('password_length');
		$email_description = '';
		if ($this->cbconfig->item('use_register_email_auth')) {
			$email_description = '회원가입 후 인증메일이 발송됩니다. 인증메일을 확인하신 후에 사이트 이용이 가능합니다';
		}

		$configbasic = array();

		$nickname_description = '';
		if ($this->cbconfig->item('change_nickname_date')) {
			$nickname_description = '<br />닉네임을 입력하시면 앞으로 '
				. $this->cbconfig->item('change_nickname_date') . '일 이내에는 변경할 수 없습니다';
		}

		$configbasic['mem_userid'] = array(
			'field' => 'mem_userid',
			'label' => '아이디',
			'rules' => 'trim|required|alphanumunder|min_length[3]|max_length[20]|is_unique[member_userid.mem_userid]|callback__mem_userid_check',
			'description' => '영문자, 숫자, _ 만 입력 가능. 최소 3자이상 입력하세요',
		);

		$password_description = '비밀번호는 ' . $password_length . '자리 이상이어야 ';
		if ($this->cbconfig->item('password_uppercase_length')
			OR $this->cbconfig->item('password_numbers_length')
			OR $this->cbconfig->item('password_specialchars_length')) {

			$password_description .= '하며 ';
			if ($this->cbconfig->item('password_uppercase_length')) {
				$password_description .= ', ' . $this->cbconfig->item('password_uppercase_length') . '개의 대문자';
			}
			if ($this->cbconfig->item('password_numbers_length')) {
				$password_description .= ', ' . $this->cbconfig->item('password_numbers_length') . '개의 숫자';
			}
			if ($this->cbconfig->item('password_specialchars_length')) {
				$password_description .= ', ' . $this->cbconfig->item('password_specialchars_length') . '개의 특수문자';
			}
			$password_description .= '를 포함해야 ';
		}
		$password_description .= '합니다';

		$configbasic['mem_password'] = array(
			'field' => 'mem_password',
			'label' => '패스워드',
			'rules' => 'trim|required|min_length[' . $password_length . ']|callback__mem_password_check',
			'description' => $password_description,
		);
		$configbasic['mem_password_re'] = array(
			'field' => 'mem_password_re',
			'label' => '패스워드 확인',
			'rules' => 'trim|required|min_length[' . $password_length . ']|matches[mem_password]',
		);
		$configbasic['mem_username'] = array(
			'field' => 'mem_username',
			'label' => '이름',
			'rules' => 'trim|min_length[2]|max_length[20]',
		);
		$configbasic['mem_nickname'] = array(
			'field' => 'mem_nickname',
			'label' => '닉네임',
			'rules' => 'trim|required|min_length[2]|max_length[20]|callback__mem_nickname_check',
			'description' => '공백없이 한글, 영문, 숫자만 입력 가능 2글자 이상' . $nickname_description,
		);
		$configbasic['mem_email'] = array(
			'field' => 'mem_email',
			'label' => '이메일',
			'rules' => 'trim|required|valid_email|max_length[50]|is_unique[member.mem_email]|callback__mem_email_check',
			'description' => $email_description,
		);
		$configbasic['mem_homepage'] = array(
			'field' => 'mem_homepage',
			'label' => '홈페이지',
			'rules' => 'prep_url|valid_url',
		);
		$configbasic['mem_phone'] = array(
			'field' => 'mem_phone',
			'label' => '전화번호',
			'rules' => 'trim|valid_phone',
		);
		$configbasic['mem_birthday'] = array(
			'field' => 'mem_birthday',
			'label' => '생년월일',
			'rules' => 'trim|exact_length[10]',
		);
		$configbasic['mem_sex'] = array(
			'field' => 'mem_sex',
			'label' => '성별',
			'rules' => 'trim|exact_length[1]',
		);
		$configbasic['mem_zipcode'] = array(
			'field' => 'mem_zipcode',
			'label' => '우편번호',
			'rules' => 'trim|min_length[5]|max_length[7]',
		);
		$configbasic['mem_address1'] = array(
			'field' => 'mem_address1',
			'label' => '기본주소',
			'rules' => 'trim',
		);
		$configbasic['mem_address2'] = array(
			'field' => 'mem_address2',
			'label' => '상세주소',
			'rules' => 'trim',
		);
		$configbasic['mem_address3'] = array(
			'field' => 'mem_address3',
			'label' => '참고항목',
			'rules' => 'trim',
		);
		$configbasic['mem_address4'] = array(
			'field' => 'mem_address4',
			'label' => '지번',
			'rules' => 'trim',
		);
		$configbasic['mem_profile_content'] = array(
			'field' => 'mem_profile_content',
			'label' => '자기소개',
			'rules' => 'trim',
		);
		$configbasic['mem_open_profile'] = array(
			'field' => 'mem_open_profile',
			'label' => '정보공개',
			'rules' => 'trim|exact_length[1]',
		);
		if ($this->cbconfig->item('use_note')) {
			$configbasic['mem_use_note'] = array(
				'field' => 'mem_use_note',
				'label' => '쪽지사용',
				'rules' => 'trim|exact_length[1]',
			);
		}
		$configbasic['mem_receive_email'] = array(
			'field' => 'mem_receive_email',
			'label' => '이메일수신여부',
			'rules' => 'trim|exact_length[1]',
		);
		$configbasic['mem_receive_sms'] = array(
			'field' => 'mem_receive_sms',
			'label' => 'SMS 문자수신여부',
			'rules' => 'trim|exact_length[1]',
		);
		$configbasic['mem_recommend'] = array(
			'field' => 'mem_recommend',
			'label' => '추천인아이디',
			'rules' => 'trim|alphanumunder|min_length[3]|max_length[20]|callback__mem_recommend_check',
		);

		if ($this->member->is_admin() === false && ! $this->session->userdata('registeragree')) {
			$this->session->set_flashdata(
				'message',
				'회원가입약관동의와 개인정보취급방침동의후 회원가입이 가능합니다'
			);
			redirect('register');
		}

		$registerform = $this->cbconfig->item('registerform');
		$form = json_decode($registerform, true);

		$config = array();
		if ($form && is_array($form)) {
			foreach ($form as $key => $value) {
				if ( ! element('use', $value)) {
					continue;
				}
				if (element('func', $value) === 'basic') {

					if ($key === 'mem_address') {
						if (element('required', $value) === '1') {
							$configbasic['mem_zipcode']['rules'] = $configbasic['mem_zipcode']['rules'] . '|required';
						}
						$config[] = $configbasic['mem_zipcode'];
						if (element('required', $value) === '1') {
							$configbasic['mem_address1']['rules'] = $configbasic['mem_address1']['rules'] . '|required';
						}
						$config[] = $configbasic['mem_address1'];
						if (element('required', $value) === '1') {
							$configbasic['mem_address2']['rules'] = $configbasic['mem_address2']['rules'] . '|required';
						}
						$config[] = $configbasic['mem_address2'];
					} else {
						if (element('required', $value) === '1') {
							$configbasic[$value['field_name']]['rules'] = $configbasic[$value['field_name']]['rules'] . '|required';
						}
						if (element('field_type', $value) === 'phone') {
							$configbasic[$value['field_name']]['rules'] = $configbasic[$value['field_name']]['rules'] . '|valid_phone';
						}
						$config[] = $configbasic[$value['field_name']];
						if ($key === 'mem_password') {
							$config[] = $configbasic['mem_password_re'];
						}
					}
				} else {
					$required = element('required', $value) ? '|required' : '';
					if (element('field_type', $value) === 'checkbox') {
						$config[] = array(
							'field' => element('field_name', $value) . '[]',
							'label' => element('display_name', $value),
							'rules' => 'trim' . $required,
						);
					} else {
						$config[] = array(
							'field' => element('field_name', $value),
							'label' => element('display_name', $value),
							'rules' => 'trim' . $required,
						);
					}
				}
			}
		}

		if ($this->cbconfig->item('use_recaptcha')) {
			$config[] = array(
				'field' => 'g-recaptcha-response',
				'label' => '자동등록방지문자',
				'rules' => 'trim|required|callback__check_recaptcha',
			);
		} else {
			$config[] = array(
				'field' => 'captcha_key',
				'label' => '자동등록방지문자',
				'rules' => 'trim|required|callback__check_captcha',
			);
		}
		$this->form_validation->set_rules($config);

		$form_validation = $this->form_validation->run();
		$file_error = '';
		$updatephoto = '';
		$file_error2 = '';
		$updateicon = '';

		if ($form_validation) {
			$this->load->library('upload');
			if ($this->cbconfig->item('use_member_photo') && $this->cbconfig->item('member_photo_width') > 0 && $this->cbconfig->item('member_photo_height') > 0) {
				if (isset($_FILES) && isset($_FILES['mem_photo']) && isset($_FILES['mem_photo']['name']) && $_FILES['mem_photo']['name']) {
					$upload_path = config_item('uploads_dir') . '/member_photo/';
					if (is_dir($upload_path) === false) {
						mkdir($upload_path, 0707);
						$file = $upload_path . 'index.php';
						$f = @fopen($file, 'w');
						@fwrite($f, '');
						@fclose($f);
						@chmod($file, 0644);
					}
					$upload_path .= cdate('Y') . '/';
					if (is_dir($upload_path) === false) {
						mkdir($upload_path, 0707);
						$file = $upload_path . 'index.php';
						$f = @fopen($file, 'w');
						@fwrite($f, '');
						@fclose($f);
						@chmod($file, 0644);
					}
					$upload_path .= cdate('m') . '/';
					if (is_dir($upload_path) === false) {
						mkdir($upload_path, 0707);
						$file = $upload_path . 'index.php';
						$f = @fopen($file, 'w');
						@fwrite($f, '');
						@fclose($f);
						@chmod($file, 0644);
					}

					$uploadconfig = array();
					$uploadconfig['upload_path'] = $upload_path;
					$uploadconfig['allowed_types'] = 'jpg|jpeg|png|gif';
					$uploadconfig['max_size'] = '2000';
					$uploadconfig['max_width'] = '1000';
					$uploadconfig['max_height'] = '1000';
					$uploadconfig['encrypt_name'] = true;

					$this->upload->initialize($uploadconfig);

					if ($this->upload->do_upload('mem_photo')) {
						$img = $this->upload->data();
						$updatephoto = cdate('Y') . '/' . cdate('m') . '/' . $img['file_name'];
					} else {
						$file_error = $this->upload->display_errors();

					}
				}
			}

			if ($this->cbconfig->item('use_member_icon') && $this->cbconfig->item('member_icon_width') > 0 && $this->cbconfig->item('member_icon_height') > 0) {
				if (isset($_FILES) && isset($_FILES['mem_icon']) && isset($_FILES['mem_icon']['name']) && $_FILES['mem_icon']['name']) {
					$upload_path = config_item('uploads_dir') . '/member_icon/';
					if (is_dir($upload_path) === false) {
						mkdir($upload_path, 0707);
						$file = $upload_path . 'index.php';
						$f = @fopen($file, 'w');
						@fwrite($f, '');
						@fclose($f);
						@chmod($file, 0644);
					}
					$upload_path .= cdate('Y') . '/';
					if (is_dir($upload_path) === false) {
						mkdir($upload_path, 0707);
						$file = $upload_path . 'index.php';
						$f = @fopen($file, 'w');
						@fwrite($f, '');
						@fclose($f);
						@chmod($file, 0644);
					}
					$upload_path .= cdate('m') . '/';
					if (is_dir($upload_path) === false) {
						mkdir($upload_path, 0707);
						$file = $upload_path . 'index.php';
						$f = @fopen($file, 'w');
						@fwrite($f, '');
						@fclose($f);
						@chmod($file, 0644);
					}

					$uploadconfig = array();
					$uploadconfig['upload_path'] = $upload_path;
					$uploadconfig['allowed_types'] = 'jpg|jpeg|png|gif';
					$uploadconfig['max_size'] = '2000';
					$uploadconfig['max_width'] = '1000';
					$uploadconfig['max_height'] = '1000';
					$uploadconfig['encrypt_name'] = true;

					$this->upload->initialize($uploadconfig);

					if ($this->upload->do_upload('mem_icon')) {
						$img = $this->upload->data();
						$updateicon = cdate('Y') . '/' . cdate('m') . '/' . $img['file_name'];
					} else {
						$file_error2 = $this->upload->display_errors();
					}
				}
			}
		}

		/**
		 * 유효성 검사를 하지 않는 경우, 또는 유효성 검사에 실패한 경우입니다.
		 * 즉 글쓰기나 수정 페이지를 보고 있는 경우입니다
		 */
		if ($form_validation === false OR $file_error !== '' OR $file_error2 !== '') {

			// 이벤트가 존재하면 실행합니다
			$view['view']['event']['formrunfalse'] = Events::trigger('formrunfalse', $eventname);

			$html_content = array();

			$k = 0;
			if ($form && is_array($form)) {
				foreach ($form as $key => $value) {
					if ( ! element('use', $value)) {
						continue;
					}

					$required = element('required', $value) ? 'required' : '';

					$html_content[$k]['field_name'] = element('field_name', $value);
					$html_content[$k]['display_name'] = element('display_name', $value);
					$html_content[$k]['input'] = '';

					//field_type : text, url, email, phone, textarea, radio, select, checkbox, date
					if (element('field_type', $value) === 'text'
						OR element('field_type', $value) === 'url'
						OR element('field_type', $value) === 'email'
						OR element('field_type', $value) === 'phone'
						OR element('field_type', $value) === 'date') {
						if (element('field_type', $value) === 'date') {
							$html_content[$k]['input'] .= '<input type="text" id="' . element('field_name', $value) . '" name="' . element('field_name', $value) . '" class="form-control input datepicker" value="' . set_value(element('field_name', $value)) . '" readonly="readonly" ' . $required . ' />';
						} elseif (element('field_type', $value) === 'phone') {
							$html_content[$k]['input'] .= '<input type="text" id="' . element('field_name', $value) . '" name="' . element('field_name', $value) . '" class="form-control input validphone" value="' . set_value(element('field_name', $value)) . '" ' . $required . ' />';
						} else {
							$html_content[$k]['input'] .= '<input type="' . element('field_type', $value) . '" id="' . element('field_name', $value) . '" name="' . element('field_name', $value) . '" class="form-control input" value="' . set_value(element('field_name', $value)) . '" ' . $required . '/>';
						}
					} elseif (element('field_type', $value) === 'textarea') {
						$html_content[$k]['input'] .= '<textarea id="' . element('field_name', $value) . '" name="' . element('field_name', $value) . '" class="form-control input" ' . $required . '>' . set_value(element('field_name', $value)) . '</textarea>';
					} elseif (element('field_type', $value) === 'radio') {
						$html_content[$k]['input'] .= '<div class="checkbox">';
						if (element('field_name', $value) === 'mem_sex') {
							$options = array(
								'1' => '남성',
								'2' => '여성',
							);
						} else {
							$options = explode("\n", element('options', $value));
						}
						$i =1;
						if ($options) {
							foreach ($options as $okey => $oval) {
								$radiovalue = (element('field_name', $value) === 'mem_sex') ? $okey : $oval;
								$html_content[$k]['input'] .= '<label for="' . element('field_name', $value) . '_' . $i . '"><input type="radio" name="' . element('field_name', $value) . '" id="' . element('field_name', $value) . '_' . $i . '" value="' . $radiovalue . '" ' . set_radio(element('field_name', $value), $radiovalue) . ' /> ' . $oval . ' </label> ';
								$i++;
							}
						}
						$html_content[$k]['input'] .= '</div>';
					} elseif (element('field_type', $value) === 'checkbox') {
						$html_content[$k]['input'] .= '<div class="checkbox">';
						$options = explode("\n", element('options', $value));
						$i =1;
						if ($options) {
							foreach ($options as $okey => $oval) {
								$html_content[$k]['input'] .= '<label for="' . element('field_name', $value) . '_' . $i . '"><input type="checkbox" name="' . element('field_name', $value) . '[]" id="' . element('field_name', $value) . '_' . $i . '" value="' . $oval . '" ' . set_checkbox(element('field_name', $value), $oval) . ' /> ' . $oval . ' </label> ';
								$i++;
							}
						}
						$html_content[$k]['input'] .= '</div>';
					} elseif (element('field_type', $value) === 'select') {
						$html_content[$k]['input'] .= '<div class="input-group">';
						$html_content[$k]['input'] .= '<select name="' . element('field_name', $value) . '" class="form-control input" ' . $required . '>';
						$html_content[$k]['input'] .= '<option value="" >선택하세요</option> ';
						$options = explode("\n", element('options', $value));
						if ($options) {
							foreach ($options as $okey => $oval) {
								$html_content[$k]['input'] .= '<option value="' . $oval . '" ' . set_select(element('field_name', $value), $oval) . ' >' . $oval . '</option> ';
							}
						}
						$html_content[$k]['input'] .= '</select>';
						$html_content[$k]['input'] .= '</div>';
					} elseif (element('field_name', $value) === 'mem_address') {
						$html_content[$k]['input'] .= '
							<label for="mem_zipcode">우편번호</label>
							<label>
								<input type="text" name="mem_zipcode" value="' . set_value('mem_zipcode') . '" id="mem_zipcode" class="form-control input" size="7" maxlength="7" ' . $required . '/>
							</label>
							<label>
								<button type="button" class="btn btn-black btn-sm" style="margin-top:0px;" onclick="win_zip(\'fregisterform\', \'mem_zipcode\', \'mem_address1\', \'mem_address2\', \'mem_address3\', \'mem_address4\');">주소 검색</button>
							</label>
							<div class="addr-line mt10">
								<label for="mem_address1">기본주소</label>
								<input type="text" name="mem_address1" value="' . set_value('mem_address1') . '" id="mem_address1" class="form-control input" placeholder="기본주소" ' . $required . ' />
							</div>
							<div class="addr-line mt10">
								<label for="mem_address2">상세주소</label>
								<input type="text" name="mem_address2" value="' . set_value('mem_address2') . '" id="mem_address2" class="form-control input" placeholder="상세주소" ' . $required . ' />
							</div>
							<div class="addr-line mt10">
								<label for="mem_address3">참고항목</label>
								<input type="text" name="mem_address3" value="' . set_value('mem_address3') . '" id="mem_address3" class="form-control input" readonly="readonly" placeholder="참고항목" />
							</div>
							<input type="hidden" name="mem_address4" value="' . set_value('mem_address4') . '" />
						';
					} elseif (element('field_name', $value) === 'mem_password') {
						$html_content[$k]['input'] .= '<input type="' . element('field_type', $value) . '" id="' . element('field_name', $value) . '" name="' . element('field_name', $value) . '" class="form-control input" minlength="' . $password_length . '" />';
					}

					$html_content[$k]['description'] = '';
					if (isset($configbasic[$value['field_name']]['description']) && $configbasic[$value['field_name']]['description']) {
						$html_content[$k]['description'] = $configbasic[$value['field_name']]['description'];
					}
					if (element('field_name', $value) === 'mem_password') {
						$k++;
						$html_content[$k]['field_name'] = 'mem_password_re';
						$html_content[$k]['display_name'] = '비밀번호 확인';
						$html_content[$k]['input'] = '<input type="password" id="mem_password_re" name="mem_password_re" class="form-control input" minlength="' . $password_length . '" />';
					}
					$k++;
				}
			}

			$view['view']['html_content'] = $html_content;
			$view['view']['open_profile_description'] = '';
			if ($this->cbconfig->item('change_open_profile_date')) {
				$view['view']['open_profile_description'] = '정보공개 설정은 ' . $this->cbconfig->item('change_open_profile_date') . '일 이내에는 변경할 수 없습니다';
			}

			$view['view']['use_note_description'] = '';
			if ($this->cbconfig->item('change_use_note_date')) {
				$view['view']['use_note_description'] = '쪽지 기능 사용 설정은 ' . $this->cbconfig->item('change_use_note_date') . '일 이내에는 변경할 수 없습니다';
			}

			$view['view']['canonical'] = site_url('register/form');

			// 이벤트가 존재하면 실행합니다
			$view['view']['event']['before_layout'] = Events::trigger('before_layout', $eventname);

			/**
			 * 레이아웃을 정의합니다
			 */
			$page_title = $this->cbconfig->item('site_meta_title_register_form');
			$meta_description = $this->cbconfig->item('site_meta_description_register_form');
			$meta_keywords = $this->cbconfig->item('site_meta_keywords_register_form');
			$meta_author = $this->cbconfig->item('site_meta_author_register_form');
			$page_name = $this->cbconfig->item('site_page_name_register_form');

			$layoutconfig = array(
				'path' => 'register',
				'layout' => 'layout',
				'skin' => 'register_form',
				'layout_dir' => $this->cbconfig->item('layout_register'),
				'mobile_layout_dir' => $this->cbconfig->item('mobile_layout_register'),
				'use_sidebar' => $this->cbconfig->item('sidebar_register'),
				'use_mobile_sidebar' => $this->cbconfig->item('mobile_sidebar_register'),
				'skin_dir' => $this->cbconfig->item('skin_register'),
				'mobile_skin_dir' => $this->cbconfig->item('mobile_skin_register'),
				'page_title' => $page_title,
				'meta_description' => $meta_description,
				'meta_keywords' => $meta_keywords,
				'meta_author' => $meta_author,
				'page_name' => $page_name,
			);
			$view['layout'] = $this->managelayout->front($layoutconfig, $this->cbconfig->get_device_view_type());
			$this->data = $view;
			$this->layout = element('layout_skin_file', element('layout', $view));
			$this->view = element('view_skin_file', element('layout', $view));

		} else {

			/**
			 * 유효성 검사를 통과한 경우입니다.
			 * 즉 데이터의 insert 나 update 의 process 처리가 필요한 상황입니다
			 */

			// 이벤트가 존재하면 실행합니다
			$view['view']['event']['formruntrue'] = Events::trigger('formruntrue', $eventname);

			$mem_level = (int) $this->cbconfig->item('register_level');
			$insertdata = array();
			$metadata = array();

			$insertdata['mem_userid'] = $this->input->post('mem_userid');
			$insertdata['mem_email'] = $this->input->post('mem_email');
			$insertdata['mem_password'] = password_hash($this->input->post('mem_password'), PASSWORD_BCRYPT);
			$insertdata['mem_nickname'] = $this->input->post('mem_nickname');
			$metadata['meta_nickname_datetime'] = cdate('Y-m-d H:i:s');
			$insertdata['mem_level'] = $mem_level;

			if (isset($form['mem_username']['use']) && $form['mem_username']['use']) {
				$insertdata['mem_username'] = $this->input->post('mem_username', null, '');
			}
			if (isset($form['mem_homepage']['use']) && $form['mem_homepage']['use']) {
				$insertdata['mem_homepage'] = $this->input->post('mem_homepage', null, '');
			}
			if (isset($form['mem_phone']['use']) && $form['mem_phone']['use']) {
				$insertdata['mem_phone'] = $this->input->post('mem_phone', null, '');
			}
			if (isset($form['mem_birthday']['use']) && $form['mem_birthday']['use']) {
				$insertdata['mem_birthday'] = $this->input->post('mem_birthday', null, '');
			}
			if (isset($form['mem_sex']['use']) && $form['mem_sex']['use']) {
				$insertdata['mem_sex'] = $this->input->post('mem_sex', null, '');
			}
			if (isset($form['mem_address']['use']) && $form['mem_address']['use']) {
				$insertdata['mem_zipcode'] = $this->input->post('mem_zipcode', null, '');
				$insertdata['mem_address1'] = $this->input->post('mem_address1', null, '');
				$insertdata['mem_address2'] = $this->input->post('mem_address2', null, '');
				$insertdata['mem_address3'] = $this->input->post('mem_address3', null, '');
				$insertdata['mem_address4'] = $this->input->post('mem_address4', null, '');
			}
			$insertdata['mem_receive_email'] = $this->input->post('mem_receive_email') ? 1 : 0;
			if ($this->cbconfig->item('use_note')) {
				$insertdata['mem_use_note'] = $this->input->post('mem_use_note') ? 1 : 0;
				$metadata['meta_use_note_datetime'] = cdate('Y-m-d H:i:s');
			}
			$insertdata['mem_receive_sms'] = $this->input->post('mem_receive_sms') ? 1 : 0;
			$insertdata['mem_open_profile'] = $this->input->post('mem_open_profile') ? 1 : 0;
			$metadata['meta_open_profile_datetime'] = cdate('Y-m-d H:i:s');
			$insertdata['mem_register_datetime'] = cdate('Y-m-d H:i:s');
			$insertdata['mem_register_ip'] = $this->input->ip_address();
			$metadata['meta_change_pw_datetime'] = cdate('Y-m-d H:i:s');
			if (isset($form['mem_profile_content']['use']) && $form['mem_profile_content']['use']) {
				$insertdata['mem_profile_content'] = $this->input->post('mem_profile_content', null, '');
			}

			if ($this->cbconfig->item('use_register_email_auth')) {
				$insertdata['mem_email_cert'] = 0;
				$metadata['meta_email_cert_datetime'] = '';
			} else {
				$insertdata['mem_email_cert'] = 1;
				$metadata['meta_email_cert_datetime'] = cdate('Y-m-d H:i:s');
			 }

			if ($updatephoto) {
				$insertdata['mem_photo'] = $updatephoto;
			}
			if ($updateicon) {
				$insertdata['mem_icon'] = $updateicon;
			}

			$mem_id = $this->Member_model->insert($insertdata);

			$useridinsertdata = array(
				'mem_id' => $mem_id,
				'mem_userid' => $this->input->post('mem_userid'),
			);
			$this->Member_userid_model->insert($useridinsertdata);

			$this->Member_meta_model->save($mem_id, $metadata);

			$nickinsert = array(
				'mem_id' => $mem_id,
				'mni_nickname' => $this->input->post('mem_nickname'),
				'mni_start_datetime' => cdate('Y-m-d H:i:s'),
			);
			$this->Member_nickname_model->insert($nickinsert);

			$extradata = array();
			if ($form && is_array($form)) {
				$this->load->model('Member_extra_vars_model');
				foreach ($form as $key => $value) {
					if ( ! element('use', $value)) {
						continue;
					}
					if (element('func', $value) === 'basic') {
						continue;
					}
					$extradata[element('field_name', $value)] = $this->input->post(element('field_name', $value), null, '');
				}
				$this->Member_extra_vars_model->save($mem_id, $extradata);
			}

			$levelhistoryinsert = array(
				'mem_id' => $mem_id,
				'mlh_from' => 0,
				'mlh_to' => $mem_level,
				'mlh_datetime' => cdate('Y-m-d H:i:s'),
				'mlh_reason' => '회원가입',
				'mlh_ip' => $this->input->ip_address(),
			);
			$this->load->model('Member_level_history_model');
			$this->Member_level_history_model->insert($levelhistoryinsert);

			$this->load->model('Member_group_model');
			$allgroup = $this->Member_group_model->get_all_group();
			if ($allgroup && is_array($allgroup)) {
				$this->load->model('Member_group_member_model');
				foreach ($allgroup as $gkey => $gval) {
					if (element('mgr_is_default', $gval)) {
						$gminsert = array(
							'mgr_id' => element('mgr_id', $gval),
							'mem_id' => $mem_id,
							'mgm_datetime' => cdate('Y-m-d H:i:s'),
						);
						$this->Member_group_member_model->insert($gminsert);
					}
				}
			}

			$this->point->insert_point(
				$mem_id,
				$this->cbconfig->item('point_register'),
				'회원가입을 축하합니다',
				'member',
				$mem_id,
				'회원가입'
			);

			$searchconfig = array(
				'{홈페이지명}',
				'{회사명}',
				'{홈페이지주소}',
				'{회원아이디}',
				'{회원닉네임}',
				'{회원실명}',
				'{회원이메일}',
				'{메일수신여부}',
				'{쪽지수신여부}',
				'{문자수신여부}',
				'{회원아이피}',
			);
			$mem_userid = $this->input->post('mem_userid', null, '');
			$mem_nickname = $this->input->post('mem_nickname', null, '');
			$mem_username = $this->input->post('mem_username', null, '');
			$mem_email = $this->input->post('mem_email', null, '');
			$receive_email = $this->input->post('mem_receive_email') ? '동의' : '거부';
			$receive_note = $this->input->post('mem_use_note') ? '동의' : '거부';
			$receive_sms = $this->input->post('mem_receive_sms') ? '동의' : '거부';
			$replaceconfig = array(
				$this->cbconfig->item('site_title'),
				$this->cbconfig->item('company_name'),
				site_url(),
				$mem_userid,
				$mem_nickname,
				$mem_username,
				$mem_email,
				$receive_email,
				$receive_note,
				$receive_sms,
				$this->input->ip_address(),
			);
			$replaceconfig_escape = array(
				html_escape($this->cbconfig->item('site_title')),
				html_escape($this->cbconfig->item('company_name')),
				site_url(),
				html_escape($mem_userid),
				html_escape($mem_nickname),
				html_escape($mem_username),
				html_escape($mem_email),
				$receive_email,
				$receive_note,
				$receive_sms,
				$this->input->ip_address(),
			);

			if ( ! $this->cbconfig->item('use_register_email_auth')) {
				if (($this->cbconfig->item('send_email_register_user') && $this->input->post('mem_receive_email'))
					OR $this->cbconfig->item('send_email_register_alluser')) {
					$title = str_replace(
						$searchconfig,
						$replaceconfig,
						$this->cbconfig->item('send_email_register_user_title')
					);
					$content = str_replace(
						$searchconfig,
						$replaceconfig_escape,
						$this->cbconfig->item('send_email_register_user_content')
					);
					$this->email->from($this->cbconfig->item('webmaster_email'), $this->cbconfig->item('webmaster_name'));
					$this->email->to($this->input->post('mem_email'));
					$this->email->subject($title);
					$this->email->message($content);
					$this->email->send();
				}
			} else {
				$vericode = array('$', '/', '.');
				$verificationcode = str_replace(
					$vericode,
					'',
					password_hash($mem_id . '-' . $this->input->post('mem_email') . '-' . random_string('alnum', 10), PASSWORD_BCRYPT)
				);

				$beforeauthdata = array(
					'mem_id' => $mem_id,
					'mae_type' => 1,
				);
				$this->Member_auth_email_model->delete_where($beforeauthdata);
				$authdata = array(
					'mem_id' => $mem_id,
					'mae_key' => $verificationcode,
					'mae_type' => 1,
					'mae_generate_datetime' => cdate('Y-m-d H:i:s'),
				);
				$this->Member_auth_email_model->insert($authdata);

				$verify_url = site_url('verify/confirmemail?user=' . $this->input->post('mem_userid') . '&code=' . $verificationcode);

				$title = str_replace(
					$searchconfig,
					$replaceconfig,
					$this->cbconfig->item('send_email_register_user_verifytitle')
				);
				$content = str_replace(
					$searchconfig,
					$replaceconfig_escape,
					$this->cbconfig->item('send_email_register_user_verifycontent')
				);

				$title = str_replace('{메일인증주소}', $verify_url, $title);
				$content = str_replace('{메일인증주소}', $verify_url, $content);

				$this->email->from($this->cbconfig->item('webmaster_email'), $this->cbconfig->item('webmaster_name'));
				$this->email->to($this->input->post('mem_email'));
				$this->email->subject($title);
				$this->email->message($content);
				$this->email->send();

				$email_auth_message = $this->input->post('mem_email') . '로 인증메일이 발송되었습니다. <br />발송된 인증메일을 확인하신 후에 사이트 이용이 가능합니다';
				$this->session->set_flashdata(
					'email_auth_message',
					$email_auth_message
				);
			}

			$emailsendlistadmin = array();
			$notesendlistadmin = array();
			$smssendlistadmin = array();
			$notesendlistuser = array();
			$smssendlistuser = array();

			$superadminlist = '';
			if ($this->cbconfig->item('send_email_register_admin')
				OR $this->cbconfig->item('send_note_register_admin')
				OR $this->cbconfig->item('send_sms_register_admin')) {
				$mselect = 'mem_id, mem_email, mem_nickname, mem_phone';
				$superadminlist = $this->Member_model->get_superadmin_list($mselect);
			}

			if ($this->cbconfig->item('send_email_register_admin') && $superadminlist) {
				foreach ($superadminlist as $key => $value) {
					$emailsendlistadmin[$value['mem_id']] = $value;
				}
			}
			if ($this->cbconfig->item('send_note_register_admin') && $superadminlist) {
				foreach ($superadminlist as $key => $value) {
					$notesendlistadmin[$value['mem_id']] = $value;
				}
			}
			if (($this->cbconfig->item('send_note_register_user') && $this->input->post('mem_use_note'))) {
				$notesendlistuser['mem_id'] = $mem_id;
			}
			if ($this->cbconfig->item('send_sms_register_admin') && $superadminlist) {
				foreach ($superadminlist as $key => $value) {
					$smssendlistadmin[$value['mem_id']] = $value;
				}
			}
			if (($this->cbconfig->item('send_sms_register_user') && $this->input->post('mem_receive_sms'))
				OR $this->cbconfig->item('send_sms_register_alluser')) {
				if ($this->input->post('mem_phone')) {
					$smssendlistuser['mem_id'] = $mem_id;
					$smssendlistuser['mem_nickname'] = $this->input->post('mem_nickname');
					$smssendlistuser['mem_phone'] = $this->input->post('mem_phone');
				}
			}

			if ($emailsendlistadmin) {
				$title = str_replace(
					$searchconfig,
					$replaceconfig,
					$this->cbconfig->item('send_email_register_admin_title')
				);
				$content = str_replace(
					$searchconfig,
					$replaceconfig_escape,
					$this->cbconfig->item('send_email_register_admin_content')
				);
				foreach ($emailsendlistadmin as $akey => $aval) {
					$this->email->clear(true);
					$this->email->from($this->cbconfig->item('webmaster_email'), $this->cbconfig->item('webmaster_name'));
					$this->email->to(element('mem_email', $aval));
					$this->email->subject($title);
					$this->email->message($content);
					$this->email->send();
				}
			}
			if ($notesendlistadmin) {
				$title = str_replace(
					$searchconfig,
					$replaceconfig,
					$this->cbconfig->item('send_note_register_admin_title')
				);
				$content = str_replace(
					$searchconfig,
					$replaceconfig_escape,
					$this->cbconfig->item('send_note_register_admin_content')
				);
				foreach ($notesendlistadmin as $akey => $aval) {
					$note_result = $this->notelib->send_note(
						$sender = 0,
						$receiver = element('mem_id', $aval),
						$title,
						$content,
						1
					);
				}
			}
			if ($notesendlistuser && element('mem_id', $notesendlistuser)) {
				$title = str_replace(
					$searchconfig,
					$replaceconfig,
					$this->cbconfig->item('send_note_register_user_title')
				);
				$content = str_replace(
					$searchconfig,
					$replaceconfig_escape,
					$this->cbconfig->item('send_note_register_user_content')
				);
				$note_result = $this->notelib->send_note(
					$sender = 0,
					$receiver = element('mem_id', $notesendlistuser),
					$title,
					$content,
					1
				);
			}
			if ($smssendlistadmin) {
				if (file_exists(APPPATH . 'libraries/Smslib.php')) {
					$this->load->library(array('smslib'));
					$content = str_replace(
						$searchconfig,
						$replaceconfig,
						$this->cbconfig->item('send_sms_register_admin_content')
					);
					$sender = array(
						'phone' => $this->cbconfig->item('sms_admin_phone'),
					);
					$receiver = array();
					foreach ($smssendlistadmin as $akey => $aval) {
						$receiver[] = array(
							'mem_id' => element('mem_id', $aval),
							'name' => element('mem_nickname', $aval),
							'phone' => element('mem_phone', $aval),
						);
					}
					$smsresult = $this->smslib->send($receiver, $sender, $content, $date = '', '회원가입알림');
				}
			}
			if ($smssendlistuser) {
				if (file_exists(APPPATH . 'libraries/Smslib.php')) {
					$this->load->library(array('smslib'));
					$content = str_replace(
						$searchconfig,
						$replaceconfig,
						$this->cbconfig->item('send_sms_register_user_content')
					);
					$sender = array(
						'phone' => $this->cbconfig->item('sms_admin_phone'),
					);
					$receiver = array();
					$receiver[] = $smssendlistuser;
					$smsresult = $this->smslib->send($receiver, $sender, $content, $date = '', '회원가입알림');
				}
			}

			$member_register_data = array(
				'mem_id' => $mem_id,
				'mrg_ip' => $this->input->ip_address(),
				'mrg_datetime' => cdate('Y-m-d H:i:s'),
				'mrg_useragent' => $this->agent->agent_string(),
				'mrg_referer' => $this->session->userdata('site_referer'),
			);
			$recommended = '';
			if ($this->input->post('mem_recommend')) {
				$recommended = $this->Member_model->get_by_userid($this->input->post('mem_recommend'), 'mem_id');
				if (element('mem_id', $recommended)) {
					$member_register_data['mrg_recommend_mem_id'] = element('mem_id', $recommended);
				} else {
					$recommended['mem_id'] = 0;
				}
			}
			$this->load->model('Member_register_model');
			$this->Member_register_model->insert($member_register_data);

			if ($this->input->post('mem_recommend')) {
				if ($this->cbconfig->item('point_recommended')) {
					// 추천인이 존재할 경우 추천된 사람
					$this->point->insert_point(
						element('mem_id', $recommended),
						$this->cbconfig->item('point_recommended'),
						$this->input->post('mem_nickname') . ' 님이 회원가입시 추천함',
						'member_recommended',
						$mem_id,
						'회원가입추천'
					);
				}
				if ($this->cbconfig->item('point_recommender')) {
					// 추천인이 존재할 경우 가입자에게
					$this->point->insert_point(
						$mem_id,
						$this->cbconfig->item('point_recommender'),
						'회원가입 추천인존재',
						'member_recommender',
						$mem_id,
						'회원가입추천인존재'
					);
				}
			}

			$this->session->set_flashdata(
				'nickname',
				$this->input->post('mem_nickname')
			);

			if ( ! $this->cbconfig->item('use_register_email_auth')) {
				$this->session->set_userdata(
					'mem_id',
					$mem_id
				);
			}

			redirect('register/result');
		}
	}


	/**
	 * 회원가입 결과 페이지입니다
	 */
	public function result()
	{
		// 이벤트 라이브러리를 로딩합니다
		$eventname = 'event_register_result';
		$this->load->event($eventname);

		$view = array();
		$view['view'] = array();

		// 이벤트가 존재하면 실행합니다
		$view['view']['event']['before'] = Events::trigger('before', $eventname);

		$this->session->keep_flashdata('nickname');
		$this->session->keep_flashdata('email_auth_message');

		if ( ! $this->session->flashdata('nickname')) {
			redirect();
		}

		// 이벤트가 존재하면 실행합니다
		$view['view']['event']['before_layout'] = Events::trigger('before_layout', $eventname);

		/**
		 * 레이아웃을 정의합니다
		 */
		$page_title = $this->cbconfig->item('site_meta_title_register_result');
		$meta_description = $this->cbconfig->item('site_meta_description_register_result');
		$meta_keywords = $this->cbconfig->item('site_meta_keywords_register_result');
		$meta_author = $this->cbconfig->item('site_meta_author_register_result');
		$page_name = $this->cbconfig->item('site_page_name_register_result');

		$layoutconfig = array(
			'path' => 'register',
			'layout' => 'layout',
			'skin' => 'register_result',
			'layout_dir' => $this->cbconfig->item('layout_register'),
			'mobile_layout_dir' => $this->cbconfig->item('mobile_layout_register'),
			'use_sidebar' => $this->cbconfig->item('sidebar_register'),
			'use_mobile_sidebar' => $this->cbconfig->item('mobile_sidebar_register'),
			'skin_dir' => $this->cbconfig->item('skin_register'),
			'mobile_skin_dir' => $this->cbconfig->item('mobile_skin_register'),
			'page_title' => $page_title,
			'meta_description' => $meta_description,
			'meta_keywords' => $meta_keywords,
			'meta_author' => $meta_author,
			'page_name' => $page_name,
		);
		$view['layout'] = $this->managelayout->front($layoutconfig, $this->cbconfig->get_device_view_type());
		$this->data = $view;
		$this->layout = element('layout_skin_file', element('layout', $view));
		$this->view = element('view_skin_file', element('layout', $view));
	}


	public function ajax_userid_check()
	{
		// 이벤트 라이브러리를 로딩합니다
		$eventname = 'event_register_ajax_userid_check';
		$this->load->event($eventname);

		$result = array();
		$this->output->set_content_type('application/json');

		// 이벤트가 존재하면 실행합니다
		Events::trigger('before', $eventname);

		$userid = trim($this->input->post('userid'));
		if (empty($userid)) {
			$result = array(
				'result' => 'no',
				'reason' => '아이디값이 넘어오지 않았습니다',
			);
			exit(json_encode($result));
		}

		if ( ! preg_match("/^([a-z0-9_])+$/i", $userid)) {
			$result = array(
				'result' => 'no',
				'reason' => '아이디는 숫자, 알파벳, _ 만 입력가능합니다',
			);
			exit(json_encode($result));
		}

		$where = array(
			'mem_userid' => $userid,
		);
		$count = $this->Member_userid_model->count_by($where);
		if ($count > 0) {
			$result = array(
				'result' => 'no',
				'reason' => '이미 사용중인 아이디입니다',
			);
			exit(json_encode($result));
		}

		if ($this->_mem_userid_check($userid) === false) {
			$result = array(
				'result' => 'no',
				'reason' => $userid . '은(는) 예약어로 사용하실 수 없는 회원아이디입니다',
			);
			exit(json_encode($result));
		}

		// 이벤트가 존재하면 실행합니다
		Events::trigger('after', $eventname);

		$result = array(
			'result' => 'available',
			'reason' => '사용 가능한 아이디입니다',
		);
		exit(json_encode($result));
	}


	public function ajax_email_check()
	{
		// 이벤트 라이브러리를 로딩합니다
		$eventname = 'event_register_ajax_email_check';
		$this->load->event($eventname);

		$result = array();
		$this->output->set_content_type('application/json');

		// 이벤트가 존재하면 실행합니다
		Events::trigger('before', $eventname);

		$email = trim($this->input->post('email'));
		if (empty($email)) {
			$result = array(
				'result' => 'no',
				'reason' => '이메일값이 넘어오지 않았습니다',
			);
			exit(json_encode($result));
		}

		if ($this->member->item('mem_email')
			&& $this->member->item('mem_email') === $email) {
			$result = array(
				'result' => 'available',
				'reason' => '사용 가능한 이메일입니다',
			);
			exit(json_encode($result));
		}

		$where = array(
			'mem_email' => $email,
		);
		$count = $this->Member_model->count_by($where);
		if ($count > 0) {
			$result = array(
				'result' => 'no',
				'reason' => '이미 사용중인 이메일입니다',
			);
			exit(json_encode($result));
		}

		if ($this->_mem_email_check($email) === false) {
			$result = array(
				'result' => 'no',
				'reason' => $email . '은(는) 예약어로 사용하실 수 없는 이메일입니다',
			);
			exit(json_encode($result));
		}

		// 이벤트가 존재하면 실행합니다
		Events::trigger('before', $eventname);

		$result = array(
			'result' => 'available',
			'reason' => '사용 가능한 이메일입니다',
		);
		exit(json_encode($result));
	}


	public function ajax_password_check()
	{
		// 이벤트 라이브러리를 로딩합니다
		$eventname = 'event_register_ajax_password_check';
		$this->load->event($eventname);

		$result = array();
		$this->output->set_content_type('application/json');

		// 이벤트가 존재하면 실행합니다
		Events::trigger('before', $eventname);

		$password = trim($this->input->post('password'));
		if (empty($password)) {
			$result = array(
				'result' => 'no',
				'reason' => '패스워드값이 넘어오지 않았습니다',
			);
			exit(json_encode($result));
		}

		if ($this->_mem_password_check($password) === false) {
			$result = array(
				'result' => 'no',
				'reason' => '패스워드는 최소 1개 이상의 숫자를 포함해야 합니다',
			);
			exit(json_encode($result));
		}

		$result = array(
			'result' => 'available',
			'reason' => '사용 가능한 패스워드입니다',
		);
		exit(json_encode($result));
	}


	public function ajax_nickname_check()
	{
		// 이벤트 라이브러리를 로딩합니다
		$eventname = 'event_register_ajax_nickname_check';
		$this->load->event($eventname);

		$result = array();
		$this->output->set_content_type('application/json');

		// 이벤트가 존재하면 실행합니다
		Events::trigger('before', $eventname);

		$nickname = trim($this->input->post('nickname'));
		if (empty($nickname)) {
			$result = array(
				'result' => 'no',
				'reason' => '닉네임값이 넘어오지 않았습니다',
			);
			exit(json_encode($result));
		}

		if ($this->member->item('mem_nickname')
			&& $this->member->item('mem_nickname') === $nickname) {
			$result = array(
				'result' => 'available',
				'reason' => '사용 가능한 닉네임입니다',
			);
			exit(json_encode($result));
		}

		$where = array(
			'mem_nickname' => $nickname,
		);
		$count = $this->Member_model->count_by($where);
		if ($count > 0) {
			$result = array(
				'result' => 'no',
				'reason' => '이미 사용중인 닉네임입니다',
			);
			exit(json_encode($result));
		}

		if ($this->_mem_nickname_check($nickname) === false) {
			$result = array(
				'result' => 'no',
				'reason' => '이미 사용중인 닉네임입니다',
			);
			exit(json_encode($result));
		}

		$result = array(
			'result' => 'available',
			'reason' => '사용 가능한 닉네임입니다',
		);
		exit(json_encode($result));
	}


	/**
	 * 회원가입시 회원아이디를 체크하는 함수입니다
	 */
	public function _mem_userid_check($str)
	{
		if (preg_match("/[\,]?{$str}/i", $this->cbconfig->item('denied_userid_list'))) {
			$this->form_validation->set_message(
				'_mem_userid_check',
				$str . ' 은(는) 예약어로 사용하실 수 없는 회원아이디입니다'
			);
			return false;
		}

		return true;
	}


	/**
	 * 회원가입시 닉네임을 체크하는 함수입니다
	 */
	public function _mem_nickname_check($str)
	{
		$this->load->helper('chkstring');
		if (chkstring($str, _HANGUL_ + _ALPHABETIC_ + _NUMERIC_) === false) {
			$this->form_validation->set_message(
				'_mem_nickname_check',
				'닉네임은 공백없이 한글, 영문, 숫자만 입력 가능합니다'
			);
			return false;
		}

		if (preg_match("/[\,]?{$str}/i", $this->cbconfig->item('denied_nickname_list'))) {
			$this->form_validation->set_message(
				'_mem_nickname_check',
				$str . ' 은(는) 예약어로 사용하실 수 없는 닉네임입니다'
			);
			return false;
		}
		$countwhere = array(
			'mem_nickname' => $str,
		);
		$row = $this->Member_model->count_by($countwhere);

		if ($row > 0) {
			$this->form_validation->set_message(
				'_mem_nickname_check',
				$str . ' 는 이미 다른 회원이 사용하고 있는 닉네임입니다'
			);
			return false;
		}

		$countwhere = array(
			'mni_nickname' => $str,
		);
		$row = $this->Member_nickname_model->count_by($countwhere);

		if ($row > 0) {
			$this->form_validation->set_message(
				'_mem_nickname_check',
				$str . ' 는 이미 다른 회원이 사용하고 있는 닉네임입니다'
			);
			return false;
		}

		return true;
	}


	/**
	 * 회원가입시 이메일을 체크하는 함수입니다
	 */
	public function _mem_email_check($str)
	{
		list($emailid, $emaildomain) = explode('@', $str);
		$denied_list = explode(',', $this->cbconfig->item('denied_email_list'));
		$emaildomain = trim($emaildomain);
		$denied_list = array_map('trim', $denied_list);
		if (in_array($emaildomain, $denied_list)) {
			$this->form_validation->set_message(
				'_mem_email_check',
				$emaildomain . ' 은(는) 사용하실 수 없는 이메일입니다'
			);
			return false;
		}

		return true;
	}


	/**
	 * 회원가입시 추천인을 체크하는 함수입니다
	 */
	public function _mem_recommend_check($str)
	{
		if( ! $str) {
			return true;
		}

		$countwhere = array(
			'mem_userid' => $str,
			'mem_denied' => 0,
		);
		$row = $this->Member_model->count_by($countwhere);

		if ($row === 0) {
			$this->form_validation->set_message(
				'_mem_recommend_check',
				$str . ' 는 존재하지 않는 추천인 회원아이디입니다'
			);
			return false;
		}

		return true;
	}


	/**
	 * 회원가입시 captcha 체크하는 함수입니다
	 */
	public function _check_captcha($str)
	{
		$captcha = $this->session->userdata('captcha');
		if ( ! is_array($captcha) OR ! element('word', $captcha) OR strtolower(element('word', $captcha)) !== strtolower($str)) {
			$this->session->unset_userdata('captcha');
			$this->form_validation->set_message(
				'_check_captcha',
				'자동등록방지코드가 잘못되었습니다'
			);
			return false;
		}
		return true;
	}


	/**
	 * 회원가입시 recaptcha 체크하는 함수입니다
	 */
	public function _check_recaptcha($str)
	{
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = array(
			'secret' => $this->cbconfig->item('recaptcha_secret'),
			'response' => $str,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, sizeof($data));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);

		$obj = json_decode($result);

		if ((string) $obj->success !== '1') {
			$this->form_validation->set_message(
				'_check_recaptcha',
				'자동등록방지코드가 잘못되었습니다'
			);
			return false;
		}

		return true;
	}


	/**
	 * 회원가입시 패스워드가 올바른 규약에 의해 입력되었는지를 체크하는 함수입니다
	 */
	public function _mem_password_check($str)
	{
		$uppercase = $this->cbconfig->item('password_uppercase_length');
		$number = $this->cbconfig->item('password_numbers_length');
		$specialchar = $this->cbconfig->item('password_specialchars_length');

		$this->load->helper('chkstring');
		$str_uc = count_uppercase($str);
		$str_num = count_numbers($str);
		$str_spc = count_specialchars($str);

		if ($str_uc < $uppercase OR $str_num < $number OR $str_spc < $specialchar) {

			$description = '비밀번호는 ';
			if ($str_uc < $uppercase) {
				$description .= ' ' . $uppercase . '개 이상의 대문자';
			}
			if ($str_num < $number) {
				$description .= ' ' . $number . '개 이상의 숫자';
			}
			if ($str_spc < $specialchar) {
				$description .= ' ' . $specialchar . '개 이상의 특수문자';
			}
			$description .= '를 포함해야 합니다';

			$this->form_validation->set_message(
				'_mem_password_check',
				$description
			);
			return false;

		}

		return true;
	}
}
