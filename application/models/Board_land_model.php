<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Board model class
 *
 * Copyright (c) CIBoard <www.ciboard.co.kr>
 *
 * @author CIBoard (develop@ciboard.co.kr)
 */

class Board_land_model extends CB_Model
{

    /**
     * 테이블명
     */
    public $_table = 'post';

    /**
     * 사용되는 테이블의 프라이머리키
     */
    public $primary_key = 'brd_id'; // 사용되는 테이블의 프라이머리키

    public $cache_prefix = 'board/board-model-get-'; // 캐시 사용시 프리픽스

    public $cache_time = 86400; // 캐시 저장시간

    function __construct()
    {
        parent::__construct();

        check_cache_dir('board');
    }

    function land_notice()
    {
        $sql ="select * from post";
        $query = $this->db->query($sql);

        return $query;
    }


}
