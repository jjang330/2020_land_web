<article class="agree__container">
    <section class="agree__box">
        <div>
            <p style="margin-left:6.0pt;margin-right:10.2pt;text-indent:-6.0pt;text-align:center;font-weight:bold;font-size:27px;"><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">&nbsp;</span><span style="font-weight:bold;font-size:27px;">오라클랜드</span><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">(</span><span style="font-weight:bold;font-size:27px;">주</span><span style="font-family:휴먼명조;font-weight:bold;font-size:27px;">)&nbsp;</span><span style="font-weight:bold;font-size:27px;">이용약관</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">1</span><span style="font-weight:bold;font-size:15px;">장 총칙</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">1</span>조 목적</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>이 이용약관은 오라클랜드<span style="font-family:함초롬바탕;">(</span>주<span style="font-family:함초롬바탕;">)(</span>이하&nbsp;<span style="font-family:함초롬바탕;">‘</span>회사<span style="font-family:함초롬바탕;">’</span>라 한다<span style="font-family:함초롬바탕;">)</span>가 제공하는 오라클랜드<span style="font-family:함초롬바탕;">(</span>주<span style="font-family:함초롬바탕;">)&nbsp;</span>사업 관련 온라인 서비스<span style="font-family:함초롬바탕;">(</span>
            <a href="www.oracleland.net"><u><span style="font-family:함초롬바탕;color:#0000ff;">www.oracleland.net</span></u></a><span style="font-family:함초롬바탕;">)&nbsp;</span>이용과 관련하여 이용조건 및 절차<span style="font-family:함초롬바탕;">,&nbsp;</span>기타 필요한 사항을 규정함을 목적으로 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">2</span>조 정의</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>이 이용약관에서 사용하는 용어의 정의는 다음과 같습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1. ‘</span>이용자<span style="font-family:함초롬바탕;">’</span>라 함은 회사의 사이트에 접속하여 이 약관에 따라 회사가 제공하는 서비스를 이용하는 자를 말합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2. ‘</span>서비스<span style="font-family:함초롬바탕;">’</span>라 함은 구현되는 단말기<span style="font-family:함초롬바탕;">(PC, TV,&nbsp;</span>휴대형단말기 등의 각종 유무선 장치를 포함<span style="font-family:함초롬바탕;">)</span>와 상관없이 이용자가 이용할 수 있는 오라클랜드<span style="font-family:함초롬바탕;">(</span>주<span style="font-family:함초롬바탕;">)&nbsp;</span>및 웹사이트&nbsp;<span style="font-family:함초롬바탕;">(www.oracleland.net)&nbsp;</span>관련 제반 서비스를 의미합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">3. ‘</span>게시물<span style="font-family:함초롬바탕;">’</span>이라 함은 서비스상에 게시한 부호<span style="font-family:함초롬바탕;">,&nbsp;</span>문자<span style="font-family:함초롬바탕;">,&nbsp;</span>음성<span style="font-family:함초롬바탕;">,&nbsp;</span>음향<span style="font-family:함초롬바탕;">,&nbsp;</span>화상<span style="font-family:함초롬바탕;">,&nbsp;</span>동영상 등의 정보 형태의 등록<span style="font-family:함초롬바탕;">,&nbsp;</span>제작된 글<span style="font-family:함초롬바탕;">,&nbsp;</span>사진 동영상 및 각종 파일과 링크 등을 의미합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">3</span>조 약관의 효력과 변경</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>이 약관은 이용자에게 공시함으로서 효력이 발생합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>회사는 사정 변경의 경우와 영업상 중요사유가 있을 때 약관을 변경할 수 있으며<span style="font-family:함초롬바탕;">,&nbsp;</span>변경된 약관은 전항과 같은 방법으로 효력이 발생합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">4</span>조 약관 외 준칙</p>
            <p>이 약관에 명시되지 않은 사항이 관계법령에 규정되어 있을 경우에는 그 규정에 따릅니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">2</span><span style="font-weight:bold;font-size:15px;">장 서비스 이용시간</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>서비스 이용시간은 업무상 또는 기술상 특별한 지장이 없는 한 연중무휴&nbsp;<span style="font-family:함초롬바탕;">1</span>일&nbsp;<span style="font-family:함초롬바탕;">24</span>시간을 원칙으로 합니다<span style="font-family:함초롬바탕;">.&nbsp;</span>단<span style="font-family:함초롬바탕;">,&nbsp;</span>사이트는 시스템 정기점검<span style="font-family:함초롬바탕;">,&nbsp;</span>증설 및 교체를 위해 사이트가 정한 날이나 시간에 서비스를 일시중단 할 수 있으며 예정된 작업으로 인한 서비스 일시 중단은 사이트의 홈페이지에 사전에 공지하오니 수시로 참고하시길 바랍니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>단<span style="font-family:함초롬바탕;">,&nbsp;</span>사이트는 다음 경우에 대하여 사전 공지나 예고 없이 서비스를 일시적 혹은 영구적으로 중단할 수 있습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">(1)&nbsp;</span>긴급한 시스템 점검<span style="font-family:함초롬바탕;">,&nbsp;</span>증설<span style="font-family:함초롬바탕;">,&nbsp;</span>교체<span style="font-family:함초롬바탕;">,&nbsp;</span>고장 혹은 오동작을 일으키는 경우</p>
            <p><span style="font-family:함초롬바탕;">(2)&nbsp;</span>국가비상사태<span style="font-family:함초롬바탕;">,&nbsp;</span>정전<span style="font-family:함초롬바탕;">,&nbsp;</span>천재지변 등의 불가항력적인 사유가 있는 경우</p>
            <p><span style="font-family:함초롬바탕;">(3)&nbsp;</span>전기통신사업법에 규정된 기간통신사업자가 전기통신 서비스를 중지한 경우</p>
            <p><span style="font-family:함초롬바탕;">(4)&nbsp;</span>서비스 이용의 폭주 등으로 정상적인 서비스 이용에 지장이 있는 경우</p>
            <p><span style="font-family:함초롬바탕;">3.&nbsp;</span>전항에 의한 서비스 중단의 경우 사이트는 사전에 공지사항 등을 통하여 회원에게 통지 합니다<span style="font-family:함초롬바탕;">.&nbsp;</span>단<span style="font-family:함초롬바탕;">,&nbsp;</span>사이트가 통제할 수 없는 사유로 발생한 서비스의 중단에 대하여 사전공지가 불가능한 경우에는 사후공지로 대신합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">3</span><span style="font-weight:bold;font-size:15px;">장 서비스에 관한 책임의 제한</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>회사는 서비스의 이용자들의 상담내용이 관리자를 제외한 제<span style="font-family:함초롬바탕;">3</span>자에게 유출되지 않도록 최선을 다해 보안을 유지하려고 노력합니다<span style="font-family:함초롬바탕;">.&nbsp;</span>그러나 다음과 같은 경우에는 상담 내용 공개 및 상실에 대하여 회사의 책임이 없습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">(1)&nbsp;</span>사용자의 부주의로 인해 상담내용이 공개되는 경우</p>
            <p><span style="font-family:함초롬바탕;">(2)&nbsp;</span>사용자가&nbsp;<span style="font-family:함초롬바탕;">'</span>삭제<span style="font-family:함초롬바탕;">'&nbsp;</span>기능을 사용하여 상담을 삭제하였을 경우</p>
            <p><span style="font-family:함초롬바탕;">(3)&nbsp;</span>천재지변이나 그 밖의 회사에서 통제할 수 없는 상황에 의하여 상담내용이 공개되거나 상담내용이 상실되었을 경우</p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>아래와 같은 상담을 신청하는 경우에는 온라인상담 전체 또는 일부 제공하지 않을 수 있습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">(1)&nbsp;</span>같은 내용의 상담을 반복하여 신청하는 경우</p>
            <p><span style="font-family:함초롬바탕;">(2)&nbsp;</span>상식에 어긋나는 표현을 사용하여 상담을 신청하는 경우</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">4</span><span style="font-weight:bold;font-size:15px;">장 의무</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">1</span>조 회사의 의무</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>회사는 특별한 사정이 없는 한 이용자가 서비스를 이용할 수 있도록 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>회사는 이 약관에서 정한 바에 따라 계속적<span style="font-family:함초롬바탕;">,&nbsp;</span>안정적으로 서비스를 제공할 의무가 있습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">3.&nbsp;</span>회사는 이용자로부터 소정의 절차에 의해 제기되는 의견에 대해서 적절한 절차를 거쳐 처리하며<span style="font-family:함초롬바탕;">,&nbsp;</span>처리 시 일정기간이 소요될 경우 이용자에게 그 사유와 처리 일정을 알려주어야 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>제<span style="font-family:함초롬바탕;">2</span>조 이용자의 의무</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>이용자는 다음 행위를 하여서는 안 됩니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">(1)&nbsp;</span>상담신청 또는 변경 시 허위내용의 기재</p>
            <p><span style="font-family:함초롬바탕;">(2)&nbsp;</span>타인의 정보도용</p>
            <p><span style="font-family:함초롬바탕;">(3)&nbsp;</span>회사에 게시된 정보의 변경</p>
            <p><span style="font-family:함초롬바탕;">(4)&nbsp;</span>회사와 기타 제<span style="font-family:함초롬바탕;">3</span>자의 저작권 등 지적재산권에 대한 침해</p>
            <p><span style="font-family:함초롬바탕;">(5)&nbsp;</span>회사 및 기타 제<span style="font-family:함초롬바탕;">3</span>자의 명예를 손상시키거나 업무를 방해하는 행위</p>
            <p><span style="font-family:함초롬바탕;">(6)&nbsp;</span>외설　또는 폭력적인 말이나 글<span style="font-family:함초롬바탕;">,&nbsp;</span>화상<span style="font-family:함초롬바탕;">,&nbsp;</span>음향<span style="font-family:함초롬바탕;">,&nbsp;</span>기타 공서양속에 반하는 정보를 회사의 사이트에 공개 또는 게시하는 행위</p>
            <p><span style="font-family:함초롬바탕;">(7)&nbsp;</span>기타 불법적이거나 부당한 행위</p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">5</span><span style="font-weight:bold;font-size:15px;">장 게시물의 삭제</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>회사는 게시판에 정보통신망이용촉진 및 정보보호 등에 관한 법률을 위반한 청소년유해매체물이 게시되어 있는 경우에는 이를 지체 없이 삭제 합니다<span style="font-family:함초롬바탕;">.&nbsp;</span>다만<span style="font-family:함초롬바탕;">, 19</span>세 이상의&nbsp;<span style="font-family:함초롬바탕;">“</span>이용자<span style="font-family:함초롬바탕;">”</span>만 이용할 수 있는 게시판은 예외로 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>회사가 운영하는 게시판 등에 게시된 정보로 인하여 법률상 이익이 침해된 자는 회사에게 당해 정보의 삭제 또는 반박내용의 게재를 요청할 수 있습니다<span style="font-family:함초롬바탕;">.&nbsp;</span>이 경우 회사는 지체 없이 필요한 조치를 취하고 이를 즉시 신청인에게 통지합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">6</span><span style="font-weight:bold;font-size:15px;">장 저작권 등의 귀속</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>회사가 작성한 게시물 또는 저작물에 대한 저작권 기타 지적재산권은 회사에 귀속합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">2.&nbsp;</span>이용자는 회사가 제공하는 서비스를 이용함으로써 얻은 정보 중 회사에 지적재산권이 귀속된 정보를 회사의 사전승낙 없이 복제<span style="font-family:함초롬바탕;">,&nbsp;</span>전송<span style="font-family:함초롬바탕;">,&nbsp;</span>출판<span style="font-family:함초롬바탕;">,&nbsp;</span>배포<span style="font-family:함초롬바탕;">,&nbsp;</span>방송 기타 방법에 의하여 영리목적으로 이용하거나 제<span style="font-family:함초롬바탕;">3</span>자에게 이용하게 하여서는 안 됩니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">3.&nbsp;</span>회사는 약정에 따라 이용자의 저작물을 사용하는 경우 당해 이용자의 허락을 받습니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p style="font-weight:bold;font-size:15px;"><span style="font-weight:bold;font-size:15px;">제</span><span style="font-family:함초롬바탕;font-weight:bold;font-size:15px;">7</span><span style="font-weight:bold;font-size:15px;">장 분쟁조정</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p><span style="font-family:함초롬바탕;">1.&nbsp;</span>본 이용약관에 규정된 것을 제외하고 발생하는 서비스 이용에 관한 제반문제에 관한 분쟁은 최대한 쌍방합의에 의해 해결하도록 한다<span style="font-family:함초롬바탕;">.</span></p>
            <p style="font-weight:bold;"><span style="font-family:함초롬바탕;">2.&nbsp;</span>서비스 이용으로 발생한 분쟁에 대해 소송이 제기될 경우 회사의 소재지를 관할하는 법원을 관할법원으로 합니다<span style="font-family:함초롬바탕;">.</span></p>
            <p><span style="font-family:함초롬바탕;">&nbsp;</span></p>
            <p>부칙</p>
            <p>이 약관은&nbsp;<span style="font-family:함초롬바탕;">20XX</span>년&nbsp;<span style="font-family:함초롬바탕;">XX</span>월&nbsp;<span style="font-family:함초롬바탕;">XX</span>일 시행합니다<span style="font-family:함초롬바탕;">.</span></p>
        </div>
    </section>
</article>