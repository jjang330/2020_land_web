
<article class="middle__box">
    <article class="board__img__container">
        <img src="/img/main/land_cs_banner.png">
    </article>
    <article class="middle__wrap">
        <section class="board__table">
            <section class="board__title__wrap">
                <div class="board__title">FAQ</div>
            </section>
            <?php foreach ($this->data['faq_content'] as $faq ){ ?>
            <article class="faq__content__wrap">
                <section class="faq__questions">
                    <div><span>Q.</span><?=$faq['questions'] ?></div>
                </section>
                <section class="faq_answer">
                    <span><?=$faq['answer']?></span>
                </section>
            </article>
            <?php } ?>
        </section>
    </article>
</article>

