
<article class="middle__box">
    <article class="board__img__container">
        <img src="/img/main/land_cs_banner.png">
    </article>
    <article class="middle__wrap">
        <section class="board__table">
            <section class="board__title__wrap">
                <div class="board__title">공지 사항</div>
                <div class="board__search">
                    <span>SEARCH</span>
                    <img src="/img/main/search_icon.png">
                </div>
            </section>
            <ul>
                <li class="board__header">
                    <span>No.</span>
                    <span>제목</span>
                    <span>글쓴이</span>
                    <span>작성일</span>
                    <span>조회</span>
                </li>
                <?php foreach ( $this->data['notice_data'] as $row) {?>
                    <li class="board__list">
                        <span><?=$row['write_num']?></span>
                        <span class="content"><?=$row['content']?></span>
                        <span><?=$row['name']?></span>
                        <span><?=$row['date']?></span>
                        <span><?=$row['count']?></span>
                    </li>
                <?php }?>
            </ul>
            <section class="table__pagination__wrap">
                <a href="">글쓰기</a>
            </section>
        </section>
    </article>

</article>

