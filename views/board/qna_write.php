<!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>




<article class="middle__box">
    <article class="board__img__container">
        <img src="/img/main/land_cs_banner.png">
    </article>
    <article class="middle__wrap">
        <form id="user__form">
            <section class="board__table">
                <section class="board__title__wrap">
                    <div class="board__title">Q&A</div>
                </section>
                <section class="qna__write__container">
                    <section class="customer__information__wrap">
                        <section class="name__box customer__information">
                            <span>* 성명</span>
                            <input type="text" name="name">
                        </section>
                        <section class="phone__box customer__information">
                            <span>* 연락처</span>
                            <input type="text" name="phone">
                        </section>
                        <section class="pw__box customer__information">
                            <span>* 비밀번호</span>
                            <input type="password" name="pw">
                        </section>
                    </section>
                    <section class="title__box">
                        <span>* 제목</span>
                        <input type="text" name="title">

                    </section>
                    <section class="content__box">
                        <span>* 내용</span>
                        <div id="summernote"></div>
                    </section>
                </section>
                <section class="submit__wrap">
                    <input class="button" type="submit" value="저장">
                    <a href="../../index.php" class="button">뒤로가기</a>
                </section>
            </section>
        </form>
    </article>

</article>

<script>
    $(document).ready(function() {
        $('#summernote').summernote();

        $("#user__form").validate({

            rules: {
                name: {
                    required: true,
                },
                pw: {
                    required: true,
                },
                phone: {
                    required: true
                },
                title: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: '* 필수 입력 사항입니다',
                },
                pw: {
                    required: '* 필수 입력 사항입니다',
                },
                phone: {
                    required: '* 필수 입력 사항입니다',
                },
                title: {
                    required: '* 필수 입력 사항입니다',
                },

                content: {
                    required: '* 필수 입렴 사항입니다'
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
