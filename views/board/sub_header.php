

<body>
<article class="container">
    <header class="header__container">
        <article class="header__logo">
            <a href="http://land.test/"><img src="/assets/images/header/land_top_logo.png"></a>
        </article>
        <article class="main__menu__box">
            <section class="main__menu__wrap">
                <section class="main__menu">
                    <a href="<?php base_url()?>/company/about_us">회사소개</a>
                </section>
                <section class="main__menu">
                    <a href="<?php base_url()?>/company/consulting">병원컨설팅</a>
                </section>
                <section class="main__menu">
                    <a href="<?php base_url()?>/company/marketing">병원마케팅</a>
                </section>
                <section class="main__menu">
                    <a href="<?php base_url()?>/company/affiliation">가맹안내</a>
                </section>
                <section class="main__menu dropdown">
                    <button class="dropbtn"><a>고객센터</a>
                        <i class=""></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="/board/b-a-1">공지사항</a>
                        <a href="/faq/faq">FAQ</a>
                        <a href="/board/b-a-2">QNA</a>
                    </div>
                </section>
            </section>
        </article>
    </header>