
<article class="middle__box">
    <article class="board__img__container">
        <img src="/assets/images/main/land_intro_banner.png">
    </article>
    <article class="middle__wrap">
        <section class="about-us__wrap">
            <section class="about-us__title">
                <h1>오라클랜드</h1>
                <div>급변하는 의료산업 환경 속에서 경쟁우위를 선점하기 위해서는 전략적인 컨설팅과 체계적 시스템은 이제 더 이상 선택이 아닌 필수입니다.</div>
                <div>이에 따라 오라클랜드는 개원부터 경영까지 병원 운영의 보다 체계화되고 전문화된 시스템 구축을 위해 설립된 병원경영지원회사((MSO, Management Service Organization)로 각 분야별 전문가들이 개원부터 마케팅, 재무, 세무, 인사관리 등 병원 운영 전반에 대한 컨설팅 서비스를 제공해드리고 있습니다.</div>
                <div>이러한 결과로 현재 오라클 네트워크는 국내 피부과 43개 지점 이외에 중국, 베트남, 중앙아시아를 비롯한 해외 42개 지점으로 구성되어 있으며, 지속적인 해외시장 진출 확대를 통해 글로벌 메디컬그룹으로서의 입지를 공고히 할 계획입니다.</div>
                <div>다가오는 변화 속에서 기회를 찾고 한 단계 더 도약할 수 있도록 오라클랜드와 함께 하시길 바랍니다. </div>
                <div>검증된 오라클 시스템으로 성공적인 개원을 도와드릴 것을 약속드립니다.</div>
            </section>
            <section class="subsidiary-Introduce__box about-us__box">
                <h2>계열사 소개</h2>
                <section class="subsidiary-Introduce__list">
                    <a href="http://www.oraclemedicalgroup.com/" target="_blank"><img src="/assets/images/main/land_banner_subs01.png" alt="코라클"></a>
                    <a href="http://www.dermamall.co.kr/" target="_blank"><img src="/assets/images/main/land_banner_subs02.png" alt="더마몰(주)"></a>
                    <a href="http://www.droracle.co.kr/" target="_blank"><img src="/assets/images/main/land_banner_subs03.png" alt="오라클코스메틱(주)"></a>
                    <a href="http://www.tensoft.kr/" target="_blank"><img src="/assets/images/main/land_banner_subs04.png" alt="텐소프트(주)"></a>
                    <a href="http://tenlaser.com/" target="_blank"><img src="/assets/images/main/land_banner_subs05.png" alt="텐텍(주)"></a>
                    <a href="#"><img src="/assets/images/main/land_banner_subs06.png" alt="celebmall"></a>
                </section>
            </section>
            <section class="oracle-history__wrap">
                <img src="/assets/images/main/land_intro_network.png">
                <h1>연혁 및 수상내역</h1>
                <section class="oracle-awards">
                    <div class="awards__list">
                        <h3 class="awards-year">2019</h3>
                        <img src="/assets/images/main/land_intro_star01.png">
                        <p>2019.04 베트남 하노이점 / 2019.05 천안쌍용점 / 2019.06 마곡점 /2019.06 안양범계점 /</p>
                        <p>2019.06 베트남 하노이 2호점 / 2019.09 중국 동관점(직영) / 2019.09 중국 구이양점/</p>
                        <p>2019.11 중국 원저우점 / 2019.11 중국 이춘점 / 2019.12 중국 칭다오점</p>
                    </div>
                    <div class="awards__list">
                        <h3>2018</h3> 
                        <p>2018.03 중국 비제점 / 2018.04 광주봉선점 / 2018.04 중국 타이저우점 /</p>
                        <p>2018.06 베트남 호치민 2호점 / 2018.07 중국 수저우점(직역)  / 2018.07 중국 텐진점 /</p>
                        <p>2018.08 중국 쿤민점  / 2018.09 대전가오점 /</p>
                    </div>
                </section>
                <section class="oracle-awards">
                    <div class="awards__list">
                        <h3>2017</h3>
                        <p>2019.04 베트남 하노이점 / 2019.05 천안쌍용점 / 2019.06 마곡점 /2019.06 안양범계점 /</p>
                        <p>2019.06 베트남 하노이 2호점 / 2019.09 중국 동관점(직영) / 2019.09 중국 구이양점/</p>
                        <p>2019.11 중국 원저우점 / 2019.11 중국 이춘점 / 2019.12 중국 칭다오점</p>
                    </div>
                    <div class="awards__list">
                        <h3 class="awards-year">2016</h3>
                        <img src="/assets/images/main/land_intro_star02.png">
                        <p>2018.03 중국 비제점 / 2018.04 광주봉선점 / 2018.04 중국 타이저우점 /</p>
                        <p>2018.06 베트남 호치민 2호점 / 2018.07 중국 수저우점(직역)  / 2018.07 중국 텐진점 /</p>
                        <p>2018.08 중국 쿤민점  / 2018.09 대전가오점 /</p>
                    </div>
                </section>
                <section class="oracle-awards">
                    <div class="awards__list">
                        <h3 class="awards-year">2015</h3>
                        <img src="/assets/images/main/land_intro_star03.png">
                        <p>2019.04 베트남 하노이점 / 2019.05 천안쌍용점 / 2019.06 마곡점 /2019.06 안양범계점 /</p>
                        <p>2019.06 베트남 하노이 2호점 / 2019.09 중국 동관점(직영) / 2019.09 중국 구이양점/</p>
                        <p>2019.11 중국 원저우점 / 2019.11 중국 이춘점 / 2019.12 중국 칭다오점</p>
                    </div>
                    <div class="awards__list">
                        <h3>2014</h3>
                        <p>2018.03 중국 비제점 / 2018.04 광주봉선점 / 2018.04 중국 타이저우점 /</p>
                        <p>2018.06 베트남 호치민 2호점 / 2018.07 중국 수저우점(직역)  / 2018.07 중국 텐진점 /</p>
                        <p>2018.08 중국 쿤민점  / 2018.09 대전가오점 /</p>
                    </div>
                </section>
                <section class="oracle-awards">
                    <div class="awards__list">
                        <h3>2013</h3>
                        <p>2019.04 베트남 하노이점 / 2019.05 천안쌍용점 / 2019.06 마곡점 /2019.06 안양범계점 /</p>
                        <p>2019.06 베트남 하노이 2호점 / 2019.09 중국 동관점(직영) / 2019.09 중국 구이양점/</p>
                        <p>2019.11 중국 원저우점 / 2019.11 중국 이춘점 / 2019.12 중국 칭다오점</p>
                    </div>
                    <div class="awards__list">
                        <h3 class="awards-year">2012</h3>
                        <img src="/assets/images/main/land_intro_star04.png">
                        <p>2018.03 중국 비제점 / 2018.04 광주봉선점 / 2018.04 중국 타이저우점 /</p>
                        <p>2018.06 베트남 호치민 2호점 / 2018.07 중국 수저우점(직역)  / 2018.07 중국 텐진점 /</p>
                        <p>2018.08 중국 쿤민점  / 2018.09 대전가오점 /</p>
                    </div>
                </section>
                <section class="oracle-awards">
                    <div class="awards__list">
                        <h3>2011</h3>
                        <p>2019.04 베트남 하노이점 / 2019.05 천안쌍용점 / 2019.06 마곡점 /2019.06 안양범계점 /</p>
                        <p>2019.06 베트남 하노이 2호점 / 2019.09 중국 동관점(직영) / 2019.09 중국 구이양점/</p>
                        <p>2019.11 중국 원저우점 / 2019.11 중국 이춘점 / 2019.12 중국 칭다오점</p>
                    </div>
                    <div class="awards__list">
                        <h3 class="awards-year">2010</h3>
                        <img src="/assets/images/main/land_intro_star05.png">
                        <p>2018.03 중국 비제점 / 2018.04 광주봉선점 / 2018.04 중국 타이저우점 /</p>
                        <p>2018.06 베트남 호치민 2호점 / 2018.07 중국 수저우점(직역)  / 2018.07 중국 텐진점 /</p>
                        <p>2018.08 중국 쿤민점  / 2018.09 대전가오점 /</p>
                    </div>
                </section>
                <section class="oracle-awards">
                    <div class="awards__list">
                        <h3>2009</h3>
                        <p>2019.04 베트남 하노이점 / 2019.05 천안쌍용점 / 2019.06 마곡점 /2019.06 안양범계점 /</p>
                        <p>2019.06 베트남 하노이 2호점 / 2019.09 중국 동관점(직영) / 2019.09 중국 구이양점/</p>
                        <p>2019.11 중국 원저우점 / 2019.11 중국 이춘점 / 2019.12 중국 칭다오점</p>
                    </div>
                    <div class="awards__list">
                        <h3>2008</h3>
                        <p>2018.03 중국 비제점 / 2018.04 광주봉선점 / 2018.04 중국 타이저우점 /</p>
                        <p>2018.06 베트남 호치민 2호점 / 2018.07 중국 수저우점(직역)  / 2018.07 중국 텐진점 /</p>
                        <p>2018.08 중국 쿤민점  / 2018.09 대전가오점 /</p>
                    </div>
                </section>
                <section class="oracle-awards">
                    <div class="awards__list">
                        <h3>2007</h3>
                        <p>2019.04 베트남 하노이점 / 2019.05 천안쌍용점 / 2019.06 마곡점 /2019.06 안양범계점 /</p>
                        <p>2019.06 베트남 하노이 2호점 / 2019.09 중국 동관점(직영) / 2019.09 중국 구이양점/</p>
                        <p>2019.11 중국 원저우점 / 2019.11 중국 이춘점 / 2019.12 중국 칭다오점</p>
                    </div>
                    <div class="awards__list">
                        <h3>2006</h3>
                        <p>2018.03 중국 비제점 / 2018.04 광주봉선점 / 2018.04 중국 타이저우점 /</p>
                        <p>2018.06 베트남 호치민 2호점 / 2018.07 중국 수저우점(직역)  / 2018.07 중국 텐진점 /</p>
                        <p>2018.08 중국 쿤민점  / 2018.09 대전가오점 /</p>
                    </div>
                </section>
            </section>
            <article class="oracle-map__wrap">
                <h1>오시는길</h1>
                <section class="map">
                    <!-- 1. 약도 노드 -->
                    <div id="daumRoughmapContainer1582256919270" class="root_daum_roughmap root_daum_roughmap_landing"></div>

                    <!-- 2. 설치 스크립트 -->
                    <script charset="UTF-8" class="daum_roughmap_loader_script" src="https://ssl.daumcdn.net/dmaps/map_js_init/roughmapLoader.js"></script>

                    <!-- 3. 실행 스크립트 -->
                    <script charset="UTF-8">
                        new daum.roughmap.Lander({
                            "timestamp" : "1582256919270",
                            "key" : "x6rx",
                            "mapWidth" : "1070",
                            "mapHeight" : "720"
                        }).render();
                    </script>
                </section>
            </article>
        </section>
    </article>
</article>

