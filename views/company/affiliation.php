
<article class="middle__box">
    <article class="board__img__container">
        <img src="/assets/images/main/land_FR_banner.png">
    </article>
    <article class="middle__wrap">
        <article class="affiliation__box">
            <section class="top__content__wrap">
                <section class="title__box">
                    <h1>가맹 혜택</h1>
                    <h2>오라클만의 특별한 혜택으로 성공적인 개원을 약속드립니다.</h2>
                </section>
                <section class="content__box">
                    <div class="content">
                        <img src="/assets/images/main/land_FR_bene01.png">
                        <div>
                            <h3>오라클피부과 브랜드</h3>
                            <p>- 국내 ．외 80여개 지점 오라클피부과 &nbsp;브랜드 사용</p>
                            <p>- 브랜드 공유로 병원 인지도 향상</p>
                        </div>
                    </div>
                    <div class="content">
                        <img src="/assets/images/main/land_FR_bene02.png">
                        <div>
                            <h3>진료시스템</h3>
                            <p>- 운영 효율성을 높인 진료시스템</p>
                            <p>- 다양한 치료 케이스 분석과 연구를 통한 지식 및 노하우 공유</p>
                        </div>
                    </div>
                    <div class="content">
                        <img src="/assets/images/main/land_FR_bene03.png">
                        <div>
                            <h3>공동마케팅</h3>
                            <p>- 공동마케팅 진행으로 비용 절감 및 마케팅 효과 극대화</p>
                        </div>
                    </div>
                    <div class="content">
                        <img src="/assets/images/main/land_FR_bene04.png">
                        <div>
                            <h3>공동구매</h3>
                            <p>- 의료물품 및 제품, 장비 등 공동구매를 통한 원가절감</p>
                            <p></p>
                        </div>
                    </div>
                </section>
            </section>
            <section class="bottom__content__wrap">
                <section class="title__box">
                    <h1>가맹절차</h1>
                    <h2>지역에 위치한 교통수단, 옥외 광고를 활용한 지역 밀착형 오프라인 광고 진행이 가능합니다.</h2>
                </section>
                <section class="step__box">
                    <?php foreach ($step__list as $step) { ?>
                        <div class="step__list">
                            <img src="/assets/images/main/<?=$step['step_img']?>">
                            <p><?=$step['step_description']?></p>
                        </div>
                    <?php }?>
                </section>
            </section>
        </article>
    </article>
</article>

