
<article class="middle__box">
    <article class="board__img__container marketing__banner">
        <img src="/assets/images/board/land_consulting_banner.png">
    </article>
    <article class="middle__wrap">
        <article class="affiliation__box">
            <section class="top__content__wrap">
                <section class="title__box">
                    <h1>개원 컨설팅</h1>
                    <h2>개원에 필요한 절차 뿐만 아니라 병원 운영 전반에 필요한 컨설팅과 서비스를 제공합니다.</h2>
                </section>
                <section class="content__box">
                    <?php foreach ($open as $open) {?>
                        <div class="content">
                            <img src="/assets/images/board/<?=$open['img']?>">
                            <div>
                                <h3 class="list__title"><?=$open['title']?></h3>
                                <p class="list__content"><?=$open['content01']?></p><br>
                            </div>
                        </div>
                    <?php }?>
                </section>
            </section>
            <section class="middle__content__box">
                <section class="title__box">
                    <h1>직원교육 컨설팅</h1>
                    <h2>이론적으로는 한계가 있는 현장에서만의 경험과 노하우를 알려드립니다.</h2>
                </section>
                <section class="content__box">
                    <?php foreach ($employee_training as $employee_training) {?>
                        <div class="content">
                            <img src="/assets/images/board/<?=$employee_training['img']?>">
                            <div>
                                <h3 class="list__title"><?=$employee_training['title']?></h3>
                                <p class="list__content"><?=$employee_training['content01']?></p><br>
                                <p class="list__content"><?=$employee_training['content02']?></p><br>
                                <p class="list__content"><?=$employee_training['content03']?></p><br>
                                <p class="list__content"><?=$employee_training['content04']?></p>
                            </div>
                        </div>
                    <?php }?>
                </section>
            </section>
            <section class="bottom__content__wrap consulting__bottom">
                <section class="title__box">
                    <h1>의료관광 컨설팅</h1>
                    <h2>외국인환자 진료를 위한 단계별 컨설팅을 진행합니다</h2>
                </section>
                <section class="content__box">
                    <?php foreach ($medical_tourism as $medical_tourism) {?>
                        <div class="content">
                            <img src="/assets/images/board/<?=$medical_tourism['img']?>">
                            <div>
                                <h3 class="list__title"><?=$medical_tourism['title']?></h3>
                                <p class="list__content"><?=$medical_tourism['content01']?></p><br>
                                <p class="list__content"><?=$medical_tourism['content02']?></p><br>
                                <p class="list__content"><?=$medical_tourism['content03']?></p><br>
                            </div>
                        </div>
                    <?php }?>
                </section>
            </section>
        </article>
    </article>
</article>

