
    <article class="contact-us__box">
        <section class="contact-us__wrap">
            <section class="contact-us__left__box">
                <section>
                    <h1>CONTACT US</h1>
                    <div>
                        서울시 강남구 선릉로 612 한일빌딩 4층
                    </div>
                    <div>
                        02-415-8974
                    </div>
                    <div>
                        skinland11@naver.com
                    </div>
                </section>
            </section>
            <section class="contact-us__right__box">
                <?php
                $attributes = array('class' => 'form-horizontal', 'name' => 'fwrite', 'id' => 'fwrite', 'onsubmit' => 'return check_form(this)');
                echo form_open_multipart(base_url().'board_write/contact_us', $attributes);
                ?>
                <!--                    <form id="fwrite" action="--><?//=base_url()?><!--write/b-a-3" method="post" name="fwrite" onsubmit="return submitContents(this)">-->
                    <input type="hidden" name="prd_id" value="10" />
                    <label for="post_username" class="label__font__size">이름</label>
                    <input type="text" name="post_username" id="user_name">
                    <label for="post_title" class="label__font__size">연락처</label>
                    <input type="text" name="post_title" id="phone">
                    <textarea name="post_content">(개원희망지 / 개원예정시기 / 전공과목 / 관련경력) &#13;&#10; 문의내용 : </textarea>
                    <input type="checkbox" name="checked" id="privacy_checkbox"><label for="privacy_checkbox" class="checkbox__label">개인정보 처리방침 동의</label>
                    <a href="index.php/agree/privacy" class="agree">자세히</a>
                    <a href="#" onclick="document.getElementById('fwrite').submit()" class="agree__detail"><img src="/assets/images/main/land_btn.png"></a>
                </form>
            </section>

        </section>
    </article>
    <article class="logo__banner__box">
        <section class="logo__banner__wrap">
            <section class="subsidiary-Introduce__logo">
                <img src="/assets/images/main/land_bottom_logo01.png">
                <img src="/assets/images/main/land_bottom_logo02.png">
                <img src="/assets/images/main/land_bottom_logo03.png">
                <img src="/assets/images/main/land_bottom_logo04.png">
                <img src="/assets/images/main/land_bottom_logo05.png">
                <img src="/assets/images/main/land_bottom_logo06.png">
                <img src="/assets/images/main/land_bottom_logo07.png">
            </section>
        </section>
    </article>
    <article class="footer__box">
        <section class="footer__wrap">
            <div class="supporter__box">
                <a href="index.php/agree/privacy">개인정보 취급방침</span></a><a href="index.php/agree/terms"><span class="border__line">이용약관</span></a><span>FAQ</span>
            </div>
            <div class="add__box"> 
                <div>오라클랜드(주) / 서울특별시 강남구 선릉로 612 한일빌딩 4층 / 대포자 : 노영우</div>
                <div>사업자등록번호 : 308-81-44202</div>
            </div>
        </section>
    </article>



</article>
</body>
    <script>
        function check_form(join) {
            var privacy_check=document.fwrite.privacy_checkbox.checked;
            if(!privacy_check){
                alert('약관1에 동의해 주세요');
                return false;
            }
        }
        console.log('123123');
    </script>
</html>