
<article class="middle__box">
    <section class="main__banner__wrap">
        <img src="/assets/images/main/land_main_banner.png">
    </section>
    <section class="middle__wrap">
        <section class="middle__banner__title">
            <span>성공하는 병원의 노하우, 오라클랜드</span>
        </section>
        <section class="top__banner__img">
            <img src="/assets/images/main/land_banner_management.png">
            <img src="/assets/images/main/land_banner_marketing.png">
            <img src="/assets/images/main/land_banner_hrd.png">
        </section>
        <section class="middle__banner__img">
            <section class="left__box box__position">
                <span>공지사항</span>
                <a href="<?php base_url()?>/board/notice"><span class="right__span">MORE</span></a>
                <section class="notice__box">
                    <ul>
<!--                    --><?php //print_r2($notice) ?>
                        <?php foreach ($notice as $list) {?>
                            <li>
                                <a href="<?=base_url()?>/post/<?=$list['write_num']?>">· <?=$list['content'] ?></a><span><?=$list['date'] ?></span>
                            </li>
                        <?php } ?>
                    </ul>
                </section>
            </section>
            <section class="center__box box__position">
                <span>오라클 네트워크</span>
                <img src="/assets/images/main/land_banner_network.png">
            </section>
            <section class="right__box box__position">
                <span>오라클 홍보영상</span>
                <img src="/assets/images/main/land_banner_intro.png">
                <img class="intro" src="/assets/images/main/land_banner_intro_btn.png">
            </section>
        </section>
        <section class="oracle-awards__box">
            <img src="/assets/images/main/land_main_awards01.png">
            <img src="/assets/images/main/land_main_awards02.png">
            <img src="/assets/images/main/land_main_awards03.png">
            <img src="/assets/images/main/land_main_awards05.png">
            <img src="/assets/images/main/land_main_awards06.png">
        </section>
        <section class="subsidiary-Introduce__box">
            <h2>계열사 소개</h2>
            <section class="subsidiary-Introduce__list">
                <a href="http://www.oraclemedicalgroup.com/" target="_blank"><img src="/assets/images/main/land_banner_subs01.png" alt="코라클"></a>
                <a href="http://www.dermamall.co.kr/" target="_blank"><img src="/assets/images/main/land_banner_subs02.png" alt="더마몰(주)"></a>
                <a href="http://www.droracle.co.kr/" target="_blank"><img src="/assets/images/main/land_banner_subs03.png" alt="오라클코스메틱(주)"></a>
                <a href="http://www.tensoft.kr/" target="_blank"><img src="/assets/images/main/land_banner_subs04.png" alt="텐소프트(주)"></a>
                <a href="http://tenlaser.com/" target="_blank"><img src="/assets/images/main/land_banner_subs05.png" alt="텐텍(주)"></a>
                <a href="#"><img src="/assets/images/main/land_banner_subs06.png" alt="celebmall"></a>
            </section>
        </section>
    </section>
</article>
