
<article class="middle__box">
    <article class="board__img__container marketing__banner">
        <img src="/assets/images/board/land_marketing_banner.png">
    </article>
    <article class="middle__wrap">
        <article class="affiliation__box">
            <section class="top__content__wrap">
                <section class="title__box">
                    <h1>온라인 마케팅</h1>
                    <h2>IT 기술과 디지털 콘텐츠를 효과적으로 사용하여 합리적인 홍보·마케팅을 진행하고 있습니다.</h2>
                </section>
                <section class="content__box">
                    <?php foreach ($online as $online) {?>
                        <div class="content">
                            <img src="/assets/images/board/<?=$online['img']?>">
                            <div>
                                <h3 class="list__title"><?=$online['title']?></h3>
                                <p class="list__content"><?=$online['content01']?></p><br>
                                <p class="list__content"><?=$online['content02']?></p><br>
                                <p class="list__content"><?=$online['content03']?></p><br>
                                <p class="list__content"><?=$online['content04']?></p>
                            </div>
                        </div>
                    <?php }?>
                </section>
            </section>
            <section class="bottom__content__wrap marketing__bottom">
                <section class="title__box">
                    <h1>오프라인 마케팅</h1>
                    <h2>지역에 위치한 교통수단, 옥외 광고를 활용한 지역 밀착형 오프라인 광고 진행이 가능합니다.</h2>
                </section>
                <section class="content__box">
                    <?php foreach ($offline as $offline) {?>
                        <div class="content">
                            <img src="/assets/images/board/<?=$offline['img']?>">
                            <div>
                                <h3 class="list__title"><?=$offline['title']?></h3>
                                <p class="list__content"><?=$offline['content01']?></p><br>
                                <p class="list__content"><?=$offline['content02']?></p><br>
                                <p class="list__content"><?=$offline['content03']?></p><br>
                                <p class="list__content"><?=$offline['content04']?></p>
                            </div>
                        </div>
                    <?php }?>
                </section>
            </section>
        </article>
    </article>
</article>

