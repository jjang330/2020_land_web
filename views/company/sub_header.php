

<body>
<article class="container">
    <header class="header__container">
        <article class="header__logo">
            <img src="/assets/images/header/land_top_logo.png">
        </article>
        <article class="main__menu__box">
            <section class="main__menu__wrap">
                <section class="main__menu">
                    <a href="<?php base_url()?>/company/about_us">회사소개</a>
                </section>
                <section class="main__menu">
                    <a href="<?php base_url()?>/company/consulting">병원컨설팅</a>
                </section>
                <section class="main__menu">
                    <a href="<?php base_url()?>/company/marketing">병원마케팅</a>
                </section>
                <section class="main__menu">
                    <a href="<?php base_url()?>/company/affiliation">가맹안내</a>
                </section>
                <section class="main__menu">
                    <a>고객센터</a>
                </section>
            </section>
        </article>
    </header>